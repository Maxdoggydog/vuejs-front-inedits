import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home/home.component.vue';
import Login from '@/views/Login/login.component.vue';
import Register from '@/views/Register/register.component.vue';
import RegisterCheckEmail from '@/views/RegisterCheckEmail/registerCheckEmail.component.vue';
import Reset from '@/views/Reset/reset.component.vue';
import ResetCheckEmail from '@/views/ResetCheckEmail/resetCheckEmail.component.vue';
import Cgu from '@/views/Cgu/cgu.component.vue';
import Page from '@/views/Page/page.component.vue';
import CommentCaMarche from '@/views/CommentCaMarche/commentCaMarche.component.vue';
import Pack from '@/views/Pack/pack.component.vue';
import Profile from '@/views/Profile/profile.component.vue';
import ProfileEdit from '@/views/Profile/profileEdit.component.vue';
import Company from '@/views/Company/company.component.vue';
import CompanyNew from '@/views/Company/company_new.component.vue';
import CompanyShow from '@/views/Company/company_show.component.vue';
import CompanyContributors from '@/views/Company/company_contributors.component.vue';
import Add from '@/views/Post/add.component.vue';
import Tree from '@/views/Post/trees.component.vue';
import TreePost from '@/views/Post/tree_posts.component.vue';
import PostShow from '@/views/Post/post_show.component.vue';
import Pdfs from '@/views/Pdf/pdfs.component.vue';
import PdfCommand from '@/views/Pdf/pdf_command.component.vue';
import Selections from '@/views/Selection/selections.component.vue';
import SelectionShow from '@/views/Selection/selection_show.component.vue';
import InitMeta from './_services/initMetaData';
import store from '@/_store';
import storeUser from '@/_store/modules/user';

Vue.use(Router);
const initMetaService = new InitMeta();
const cguMeta = initMetaService.initRouterMeta('cgu');
const commentCeMarcheMeta = initMetaService.initRouterMeta('comment-ca-marche');
const homeMeta = initMetaService.initRouterMeta('home');
const loginMeta = initMetaService.initRouterMeta('login');
const registerMeta = initMetaService.initRouterMeta('register');
const resetMeta = initMetaService.initRouterMeta('reset');
const resetCheckEmailMeta = initMetaService.initRouterMeta('reset-check-email');
const registerCheckEmailMeta = initMetaService.initRouterMeta('register-check-email');
const packMeta = initMetaService.initRouterMeta('pack');
const user = storeUser.state.user;
const profileMeta = initMetaService.initRouterMeta('profile', user);
const profileEditMeta = initMetaService.initRouterMeta('profile-edit');
const companyMeta = initMetaService.initRouterMeta('company');
const companyNewMeta = initMetaService.initRouterMeta('company-new');
const companyShowMeta = initMetaService.initRouterMeta('company-show');
const companyContributorsMeta = initMetaService.initRouterMeta('company-contributors');
const treeMeta = initMetaService.initRouterMeta('tree');
const treePostsMeta = initMetaService.initRouterMeta('tree-posts');
const postShowMeta = initMetaService.initRouterMeta('post-show', user);
const addMeta = initMetaService.initRouterMeta('add');
const pdfsMeta = initMetaService.initRouterMeta('pdfs');
const pdfCommandMeta = initMetaService.initRouterMeta('pdf-command');
const selectionMeta = initMetaService.initRouterMeta('selection');
const selectionShowMeta = initMetaService.initRouterMeta('selection-show');

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        title: homeMeta.title,
        metaTags: homeMeta.metaTags,
      },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        pageSentence: loginMeta.pageSentence,
        title: loginMeta.title,
        metaTags: loginMeta.metaTags,
      },
    },
    {
      path: '/register',
      name: 'register',
      component: Register,
      meta: {
        pageSentence: registerMeta.pageSentence,
        title: registerMeta.title,
        metaTags: registerMeta.metaTags,
      },
    },
    {
      path: '/register/check-email',
      name: 'register-check-email',
      component: RegisterCheckEmail,
      meta: {
        pageSentence: registerCheckEmailMeta.pageSentence,
        title: registerCheckEmailMeta.title,
        metaTags: registerCheckEmailMeta.metaTags,
      },
    },
    {
      path: '/resetting/request',
      name: 'reset',
      component: Reset,
      meta: {
        pageSentence: resetMeta.pageSentence,
        title: resetMeta.title,
        metaTags: resetMeta.metaTags,
      },
    },
    {
      path: '/resetting/check-email',
      name: 'reset-check-email',
      component: ResetCheckEmail,
      meta: {
        pageSentence: resetCheckEmailMeta.pageSentence,
        title: resetCheckEmailMeta.title,
        metaTags: resetCheckEmailMeta.metaTags,
      },
    },
    {
      path: '/prestations',
      name: 'prestations',
      component: Pack,
      meta: {
        pageSentence: packMeta.pageSentence,
        title: packMeta.title,
        metaTags: packMeta.metaTags,
      },
    },
    {
      path: '/profile/:slug',
      name: 'profile',
      component: Profile,
      meta: {
        pageSentence: profileMeta.pageSentence,
        title: profileMeta.title,
        metaTags: profileMeta.metaTags,
      },
    },
    {
      path: '/profile/:slug/edit',
      name: 'profile-edit',
      component: ProfileEdit,
      meta: {
        pageSentence: profileEditMeta.pageSentence,
        title: profileEditMeta.title,
        metaTags: profileEditMeta.metaTags,
      },
    },
    {
      path: '/companies',
      name: 'companies',
      component: Company,
      meta: {
        pageSentence: companyMeta.pageSentence,
        title: companyMeta.title,
        metaTags: companyMeta.metaTags,
      },
    },
    {
      path: '/company/new',
      name: 'company_new',
      component: CompanyNew,
      meta: {
        pageSentence: companyNewMeta.pageSentence,
        title: companyNewMeta.title,
        metaTags: companyNewMeta.metaTags,
      },
    },
    {
      path: '/company/:slug',
      name: 'company_show',
      component: CompanyShow,
      meta: {
        pageSentence: companyShowMeta.pageSentence,
        title: companyShowMeta.title,
        metaTags: companyShowMeta.metaTags,
      },
    },
    {
      path: '/company/:slug/contributors',
      name: 'company_contributors',
      component: CompanyContributors,
      meta: {
        pageSentence: companyContributorsMeta.pageSentence,
        title: companyContributorsMeta.title,
        metaTags: companyContributorsMeta.metaTags,
      },
    },
    {
      path: '/trees',
      name: 'trees',
      component: Tree,
      meta: {
        pageSentence: treeMeta.pageSentence,
        title: treeMeta.title,
        metaTags: treeMeta.metaTags,
      },
    },
    {
      path: '/tree/:post_id',
      name: 'post_tree_show',
      component: TreePost,
      meta: {
        pageSentence: treePostsMeta.pageSentence,
        title: treePostsMeta.title,
        metaTags: treePostsMeta.metaTags,
      },
    },
    {
      path: '/tree/show/:post_id',
      name: 'post_show',
      component: PostShow,
      meta: {
        pageSentence: postShowMeta.pageSentence,
        title: postShowMeta.title,
        metaTags: postShowMeta.metaTags,
      },
    },
    {
      path: '/tree/:company_id/initialiser/:parent_id?',
      name: 'add_post',
      component: Add,
      meta: {
        pageSentence: addMeta.pageSentence,
        title: addMeta.title,
        metaTags: addMeta.metaTags,
      },
    },
    {
      path: '/selections/:post_id',
      name: 'selections',
      component: Selections,
      meta: {
        pageSentence: selectionMeta.pageSentence,
        title: selectionMeta.title,
        metaTags: selectionMeta.metaTags,
      },
    },
    {
      path: '/selections/:post_id/selection',
      name: 'selection_show',
      component: SelectionShow,
      meta: {
        pageSentence: selectionShowMeta.pageSentence,
        title: selectionShowMeta.title,
        metaTags: selectionShowMeta.metaTags,
      },
    },
    {
      path: '/selections/:post_id/edit/:pdf_id',
      name: 'selection_edit',
      component: SelectionShow,
      meta: {
        pageSentence: selectionShowMeta.pageSentence,
        title: selectionShowMeta.title,
        metaTags: selectionShowMeta.metaTags,
      },
    },
    {
      path: '/pdfs',
      name: 'pdfs',
      component: Pdfs,
      meta: {
        pageSentence: pdfsMeta.pageSentence,
        title: pdfsMeta.title,
        metaTags: pdfsMeta.metaTags,
      },
    },

    {
      path: '/pdfs/:slug/command',
      name: 'pdf_command',
      component: PdfCommand,
      meta: {
        pageSentence: pdfCommandMeta.pageSentence,
        title: pdfCommandMeta.title,
        metaTags: pdfCommandMeta.metaTags,
      },
    },
    {
      path: '/page',
      component: Page,
      children: [
        {
          path: 'cgu',
          name: 'cgu',
          component: Cgu,
          meta: {
            pageSentence: cguMeta.pageSentence,
            title: cguMeta.title,
            metaTags: cguMeta.metaTags,
          },
        },
        {
          path: 'comment-ca-marche',
          name: 'comment-ca-marche',
          component: CommentCaMarche,
          meta: {
            pageSentence: commentCeMarcheMeta.pageSentence,
            title: commentCeMarcheMeta.title,
            metaTags: commentCeMarcheMeta.metaTags,
          },
        },
      ],
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('./views/About.vue'),
    },
    {
      path: '**', redirect: '/',
      component: Home,
    },
  ],
});

