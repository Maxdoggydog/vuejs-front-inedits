import User from '@/_classes/user.class.ts';
import Mailing from '@/_classes/mailing.class.ts';
import Post from '@/_classes/post.class.ts';
import Pack from '@/_classes/pack.class.ts';

export default class Company {
  public id: any = 0;
  public name: string = '';
  public primary_color: string = '';
  public secondary_color: string = '';
  public logo: string = '';
  public token: string = '';
  public start_date: string = '';
  public end_date: string = '';
  public commanded: string = '';
  public slug: string = '';
  public pack: Pack = new Pack();
  public mails: Mailing[] = [];
  public users: User[] = [];
  public posts: Post[] = [];
  public user_profile: User = new User();

  public setCompany(company: this): Company {
    this.id = company.id;
    this.name = company.name;
    this.primary_color = company.primary_color;
    this.secondary_color = company.secondary_color;
    this.logo = company.logo;
    this.token = company.token;
    this.start_date = company.start_date;
    this.end_date = company.end_date;
    this.commanded = company.commanded;
    this.slug = company.slug;
    this.pack = company.pack;
    this.mails = company.mails;
    this.users = company.users;
    this.posts = company.posts;
    this.user_profile = company.user_profile;

    return this;
  }

}
