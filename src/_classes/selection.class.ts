import User from '@/_classes/user.class.ts';
import Post from '@/_classes/post.class.ts';

export default class Selection {
  public id: any = 0;
  public order_post: number = 0;
  public post: Post = new Post();
  public user_profile: User = new User();

  public setSelection(selection: this): Selection {
    this.id = selection.id;
    this.order_post = selection.order_post;
    this.post = selection.post;
    this.user_profile = selection.user_profile;

    return this;
  }

}
