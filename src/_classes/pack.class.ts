import PackService from '@/_services/packService';

const _packService = new PackService();
export default class Pack {
  public id: any = 0;
  public name: string = '';
  public type: string = '';
  public price: number = 0;
  public logo: string = '';
  public price_extention: string = '';
  public description: string = '';
  public description_purified: string = '';
  public type_duration: number = 0;
  public nb_page: number = 0;
  public nb_page_max: number = 0;
  public nb_photo: number = 0;
  public nb_max_users: number = 0;
  public color: boolean = false;
  public visible: boolean = true;
  public type_expedition: number = 0;
  public type_produit_reliure: number = 0;
  public type_produit_format: number = 0;
  public type_produit_imprInter: number = 0;
  public type_produit_papier_couv: number = 0;
  public type_produit_papier_inter: number = 0;
  public slug: string = '';

  public setPack(pack: this): Pack {
    this.id = pack.id;
    this.name = pack.name;
    this.type = pack.type;
    this.price = pack.price;
    this.logo = pack.logo;
    this.price_extention = pack.price_extention;
    this.description = pack.description;
    this.description_purified = pack.description_purified;
    this.type_duration = pack.type_duration;
    this.nb_page = pack.nb_page;
    this.nb_page_max = pack.nb_page_max;
    this.nb_photo = pack.nb_photo;
    this.nb_max_users = pack.nb_max_users;
    this.color = pack.color;
    this.visible = pack.visible;
    this.type_expedition = pack.type_expedition;
    this.type_produit_reliure = pack.type_produit_reliure;
    this.type_produit_format = pack.type_produit_format;
    this.type_produit_imprInter = pack.type_produit_imprInter;
    this.type_produit_papier_couv = pack.type_produit_papier_couv;
    this.type_produit_papier_inter = pack.type_produit_papier_inter;
    this.slug = pack.slug;

    return this;
  }

  /**
   * Return label of paper type
   */
  public getTypePaper() {
    return _packService.typePaper[this.type_produit_papier_inter];
  }

  /**
   * Return label of 'couverture' type
   */
  public getTypeCouverture() {
    return _packService.typeCouverture[this.type_produit_papier_couv];
  }

  /**
   * Return label of 'reliure' type
   */
  public getTypeReliure() {
    return _packService.typeReliure[this.type_produit_reliure];
  }

  /**
   * Return label of 'format' type
   */
  public getTypeFormat() {
    return _packService.typeFormat[this.type_produit_format];
  }

}
