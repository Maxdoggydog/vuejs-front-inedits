import User from '@/_classes/user.class.ts';
import Post from '@/_classes/post.class.ts';

export default class Pdf {
  public id: any = 0;
  public title: string = '';
  public text: string = '';
  public commanded: string = '';
  public slug: string = '';
  public post: Post = new Post();
  public user_profile: User = new User();

  public setPdf(pdf: this): Pdf {
    this.id = pdf.id;
    this.title = pdf.title;
    this.text = pdf.text;
    this.commanded = pdf.commanded;
    this.slug = pdf.slug;
    this.post = pdf.post;
    this.user_profile = pdf.user_profile;

    return this;
  }

}
