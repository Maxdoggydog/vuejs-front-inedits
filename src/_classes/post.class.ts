import User from '@/_classes/user.class.ts';
import Status from '@/_classes/status.class.ts';
import Company from '@/_classes/company.class.ts';
import Category from '@/_classes/category.class.ts';
import moment from 'moment';

export default class Post {
  public id: any = 0;
  public title: string = '';
  public content: string = '';
  public rules: string = '';
  public letter_count: number = 0;
  public is_main: boolean = false;
  public is_end: boolean = true;
  public slug: string = '';
  public content_plain: string = '';
  public content_purified: string = '';
  public mail_accepted: boolean = false;
  public mail_denied: boolean = false;
  public is_enable: boolean = true;
  public depth: number = 0;
  public numchild: number = 0;
  public path: string = '';
  public children_count: number = 0;
  public created_at: string = '';
  public user_profile: User = new User();
  public category: Category = new Category();
  public status: Status = new Status();
  public company: Company = new Company();
  public parents: any[] = [];
  public parent_id: any = {};
  public root_id: any = {};

  public setPost(post: this): Post {
    this.id = post.id;
    this.title = post.title;
    this.content = post.content;
    this.rules = post.rules;
    this.letter_count = post.letter_count;
    this.is_main = post.is_main;
    this.is_end = post.is_end;
    this.slug = post.slug;
    this.content_plain = post.content_plain;
    this.content_purified = post.content_purified;
    this.mail_accepted = post.mail_accepted;
    this.mail_denied = post.mail_denied;
    this.is_enable = post.is_enable;
    this.depth = post.depth;
    this.numchild = post.numchild;
    this.path = post.path;
    this.children_count = post.children_count;
    this.created_at = post.created_at;
    this.user_profile = post.user_profile;
    this.category = post.category;
    this.status = post.status;
    this.company = post.company;
    this.parents = post.parents;
    this.parent_id = post.parent_id;
    this.root_id = post.root_id;

    return this;
  }

  public getCreatedAt(format: string) {
    return moment(this.created_at).format(format);
  }

}
