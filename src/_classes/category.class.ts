export default class Category {
  public id: any = 0;
  public name: string = '';
  public color: string = '';

  public setCategory(category: this): Category {
    this.id = category.id;
    this.name = category.name;
    this.color = category.color;

    return this;
  }

}
