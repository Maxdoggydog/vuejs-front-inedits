export default class User {
  public id: any = 0;
  public experience: string = '';
  public inspiration: string = '';
  public style: string = '';
  public favoriteBook: string = '';
  public favoriteAuthors: string = '';
  public favoriteGenre: string = '';
  public cover: string = '';
  public avatar: string = '';
  public covers: string = '';
  public avatars: string = '';
  public slug: string = '';
  public pro_acces: boolean = false;
  public user: any = {email: '', first_name: '', last_name: '', id: ''};
  public facebook: string = '';
  public twitter: string = '';
  public website: string = '';

  public setProfile(profile: this): User {
    this.id = profile.id;
    this.experience = profile.experience;
    this.inspiration = profile.inspiration;
    this.style = profile.style;
    this.favoriteBook = profile.favoriteBook;
    this.favoriteAuthors = profile.favoriteAuthors;
    this.favoriteGenre = profile.favoriteGenre;
    this.cover = profile.cover;
    this.avatar = profile.avatar;
    this.slug = profile.slug;
    this.pro_acces = profile.pro_acces;
    this.user = profile.user;
    this.facebook = profile.facebook;
    this.twitter = profile.twitter;
    this.website = profile.website;

    return this;
  }

  public isFilled(part: string) {
    switch (part) {
      case 'user':
        if (this.user.experience || this.user.inspiration || this.user.style || this.user.favoriteBook || this.user.favoriteAuthors || this.user.favoriteGenre) {
          return true;
        }
      case 'social':
        if (this.user.facebook || this.user.twitter || this.user.website) {
          return true;
        }
      default:
        return false;
    }
  }

  public fullName() {
    return this.user.first_name + ' ' + this.user.last_name;
  }
}
