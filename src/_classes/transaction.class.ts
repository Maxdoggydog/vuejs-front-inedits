import Company from '@/_classes/company.class.ts';

export default class Transaction {
  public id: any = 0;
  public description: string = '';
  public amount: string = '';
  public token: string = '';
  public start: string = '';
  public end: string = '';
  public status: string = '';
  public response: string = '';
  public user_profile: any = {};
  public company: Company = new Company();

  public setTransaction(transaction: this): Transaction {
    this.id = transaction.id;
    this.description = transaction.description;
    this.amount = transaction.amount;
    this.token = transaction.token;
    this.start = transaction.start;
    this.end = transaction.end;
    this.status = transaction.status;
    this.response = transaction.response;
    this.user_profile = transaction.user_profile;
    this.company = transaction.company;

    return this;
  }

}
