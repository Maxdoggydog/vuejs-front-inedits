import Company from '@/_classes/company.class.ts';

export default class Mailing {
  public id: any = 0;
  public email: string = '';
  public company: Company = new Company();

  public setMailing(mailing: this): Mailing {
    this.id = mailing.id;
    this.email = mailing.email;
    this.company = mailing.company;

    return this;
  }

}
