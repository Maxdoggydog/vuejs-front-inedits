import Post from '@/_classes/post.class.ts';

export default class Abuse {
  public id: any = 0;
  public firstname: string = '';
  public lastname: string = '';
  public email: string = '';
  public comment: string = '';
  public post: Post = new Post();

  public setAbuse(abuse: this): Abuse {
    this.id = abuse.id;
    this.firstname = abuse.firstname;
    this.lastname = abuse.lastname;
    this.email = abuse.email;
    this.comment = abuse.comment;
    this.post = abuse.post;

    return this;
  }

}
