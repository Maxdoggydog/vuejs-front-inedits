export default class Status {
  public id: any = 0;
  public label: string = '';

  public setStatus(status: this): Status {
    this.id = status.id;
    this.label = status.label;

    return this;
  }

}
