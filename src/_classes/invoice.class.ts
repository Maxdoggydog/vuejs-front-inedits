import Transaction from '@/_classes/transaction.class.ts';

export default class Invoice {
  public id: any = 0;
  public customer_address: string = '';
  public customer_full_name: string = '';
  public pack_price: string = '';
  public pack_description: string = '';
  public transaction: Transaction = new Transaction();

  public setInvoice(invoice: this): Invoice {
    this.id = invoice.id;
    this.customer_address = invoice.customer_address;
    this.customer_full_name = invoice.customer_full_name;
    this.pack_price = invoice.pack_price;
    this.pack_description = invoice.pack_description;
    this.transaction = invoice.transaction;

    return this;
  }

}
