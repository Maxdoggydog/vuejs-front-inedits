import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import avatarVue from '@/views/templates/_avatar.component.vue';
import breadCrumbVue from '@/views/Post/_breadcrumb.component.vue';
import Post from '@/_classes/post.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storePost from '@/_store/modules/post';
import PostService from '@/_services/postService';
import TinyMCEService from '@/_services/tinyMCEService';
import Editor from '@tinymce/tinymce-vue';
import moment from 'moment';

const _postService = new PostService();
const _tinyMCEService = new TinyMCEService();
Vue.use(VueRouter);

@Component({
  components: {
    avatarVue,
    breadCrumbVue,
    editor: Editor,
  },
  name: 'postShowVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
    }),
  },
})
export default class PostShowVue extends Vue {
  public config: object = _tinyMCEService.config;
  public api_key: string = _tinyMCEService.api_key;
  public host: string = process.env.VUE_APP_API_DOMAINE;
  public post: Post = new Post();

  public mounted() {
    this.load();
  }

  get user() {
    return storeUser.state.user;
  }

  public getCreatedAt(date: any, format: string) {
    return moment(date).format(format);
  }

  public load() {
    const post_id = this.$route.params.post_id;
    _postService.getPostById(post_id)
      .then((res: any) => {
        this.post.setPost(res.data);
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur le post actuelle',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  public editPost(e: any) {
    e.preventDefault();

    const check: any = _postService.checkEditPost(this.post);

    if (check.value) {
      _postService.editPost(this.post)
        .then((res: any) => {
          console.log('res', res);
          this.post.content_purified = this.post.content;
          this.$notify({
            group: 'return-database',
            type: 'success',
            duration: -1,
            title: 'Post',
            text: 'Post édité',
          });
        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Edit post',
            text: 'Un problème c\'est déclarer coté base de donnée',
          });
        });
    } else {
      console.log('check', check);
      this.$notify({
        group: 'return-database',
        type: 'warn',
        duration: -1,
        title: 'Edit post',
        text: 'Le formulaire doit être bien rempli',
      });
    }
  }

}
