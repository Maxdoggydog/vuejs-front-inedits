import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import Company from '@/_classes/company.class.ts';
import Post from '@/_classes/post.class.ts';
import User from '@/_classes/user.class.ts';
import store from '@/_store/index';
import storeUser from '@/_store/modules/user';
import { mapState } from 'vuex';
import PostService from '@/_services/postService';
import TinyMCEService from '@/_services/tinyMCEService';
import Editor from '@tinymce/tinymce-vue';

const _postService = new PostService();
const _tinyMCEService = new TinyMCEService();
Vue.use(VueRouter);
@Component({
  components: {
    editor: Editor,
  },
  name: 'addVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
    }),
  },
})
export default class AddVue extends Vue {
  public parent: Post = new Post();
  public post: Post = new Post();
  public tree: Post = new Post();
  public show: boolean = false;
  public config: object = _tinyMCEService.config;
  public api_key: string = _tinyMCEService.api_key;

  public showTexTare() {
    this.show = !this.show;
  }

  get user() {
    return storeUser.state.user;
  }

  public createPost(e: any) {
    e.preventDefault();
    const check: any = _postService.checkPostInfo(this.post);
    if (check.value) {
      const company_id = this.$route.params.company_id;
      const parent_id = this.$route.params.parent_id;
      _postService.createPost(check.data, company_id, parent_id)
        .then((res: any) => {
          console.log('res', res);
          const parent = res.parent;
          this.$router.push({ name: 'post_tree_show', params: {company_id, post_id: parent_id }});
        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Groupe',
            text: 'Un problème c\'est déclarer coté base de donnée',
          });
        });
    } else {
      console.log('check', check);
      this.$notify({
        group: 'return-database',
        type: 'warn',
        duration: -1,
        title: 'Groupe',
        text: 'Le formulaire doit être bien rempli',
      });
    }
  }
}
