import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import avatarVue from '@/views/templates/_avatar.component.vue';
import User from '@/_classes/user.class.ts';
import store from '@/_store/index';
import Post from '@/_classes/post.class.ts';

Vue.use(VueRouter);
@Component({
  components: {
    avatarVue,
  },
  name: 'postBoxVue',
})
export default class PostBoxVue extends Vue {
  @Prop() private post!: Post;
  @Prop() private user!: User;
}
