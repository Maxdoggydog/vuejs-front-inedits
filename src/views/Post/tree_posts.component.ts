import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import avatarVue from '@/views/templates/_avatar.component.vue';
import Post from '@/_classes/post.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storePost from '@/_store/modules/post';
import PostService from '@/_services/postService';
import CoolTreeService from '@/_services/coolTreeService';

const _postService = new PostService();
const _coolTreeService = new CoolTreeService();
Vue.use(VueRouter);

@Component({
  components: {
    avatarVue,
  },
  name: 'treePostsVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      state: (state: any) => state,
    }),
  },
})
export default class TreePostsVue extends Vue {
  public host: string = process.env.VUE_APP_API_DOMAINE;
  public tree: Post = new Post();
  public parent: Post = new Post();
  public posts: Post[] = [];

  public mounted() {
    this.load();
    this.init();
  }

  get user() {
    return storeUser.state.user;
  }

  public load() {
    const post_id = this.$route.params.post_id;
    _postService.getParent(post_id)
      .then((res: any) => {
        this.tree.setPost(res.data.tree);
        this.parent.setPost(res.data.parent);
        this.posts = res.data.posts;
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la Compagnie',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  public init() {
    const post_id = this.$route.params.post_id;
    const currentUserId: any = this.user.user.id;
    const labelJSON: any   = process.env.VUE_APP_API_DOMAINE + '/pro/posts/tree/' + post_id + '/json';

    _coolTreeService.init({currentUserId, labelJSON});

  }

}
