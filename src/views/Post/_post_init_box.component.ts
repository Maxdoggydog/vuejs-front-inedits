import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import Company from '@/_classes/company.class.ts';
import store from '@/_store/index';

Vue.use(VueRouter);
@Component({
  components: {

  },
  name: 'postInitBoxVue',
})
export default class PostInitBoxVue extends Vue {
  @Prop() private company!: Company;
}
