import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import avatarVue from '@/views/templates/_avatar.component.vue';
import Post from '@/_classes/post.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storePost from '@/_store/modules/post';
import PostService from '@/_services/postService';

const _postService = new PostService();

@Component({
  components: {

  },
  name: 'breadCrumbVue',
})
export default class BreadCrumbVue extends Vue {
  @Prop() private post!: Post;
}
