import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import Company from '@/_classes/company.class.ts';
import Post from '@/_classes/post.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storeCompany from '@/_store/modules/company';
import postBoxVue from '@/views/Post/_post_box.component.vue';
import postInitBoxVue from '@/views/Post/_post_init_box.component.vue';
import CompanyService from '@/_services/companyService';
import PostService from '@/_services/postService';

const _companyService = new CompanyService();
const _postService = new PostService();
Vue.use(VueRouter);

@Component({
  components: {
    postBoxVue,
    postInitBoxVue,
  },
  name: 'treesVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      companies: (state: any) => state.company.companies,
      state: (state: any) => state,
    }),
  },
})
export default class TreesVue extends Vue {
  public posts: Post[] = [];
  public companiesWithNoTree: Company[] = [];
  public mounted() {
    this.loadCompaniesWithNoTree();
    this.loadPostRoot();
  }

  get companies() {
    return storeCompany.state.companies;
  }

  get user() {
    return storeUser.state.user;
  }

  public loadCompaniesWithNoTree() {
    _companyService.getCompanieWithoutPost()
      .then((res: any) => {
        const data = res.data;
        this.companiesWithNoTree = data.companies;
      })
      .catch((error: any) => {
        const response = error.response;
        const data = response.data;
        console.log('error', error);
        console.log('data.message', data.message);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la Compagnie',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  public loadPostRoot() {
    _postService.getRootPostByUser(this.user.user.id)
      .then((res: any) => {
        const data = res.data;
        this.posts = data.posts;
        console.log('this.posts', this.posts);
      })
      .catch((error: any) => {
        const response = error.response;
        const data = response.data;
        console.log('error', error);
        console.log('data.message', data.message);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la Compagnie',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

}
