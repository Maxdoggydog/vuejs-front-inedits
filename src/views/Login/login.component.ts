import { Component, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import { mapState } from 'vuex';
import store from '@/_store';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

Vue.use(VueRouter);
import playerVue from '@/views/templates/_player.component.vue';

@Component({
  components: {
    playerVue,
  },
  computed: mapState({
    user: 'user',
  }),
})
export default class Login extends Vue {
  public login: any = {};

  public logIn(e: any) {
    e.preventDefault();
    _logInService.getLogIn(this.login)
      .then((res: any) => {
        console.log('res', res);
        const data: any = res.data;
        const user: any = data.userprofile;
        const token: string = data.token;
        if (user && token) {
          user.token = token;
          localStorage.setItem('user', JSON.stringify(user));
          this.$store.dispatch('login', user);
          // this.$parent.$router.push('/');
          document.location.href = '/'; // ==> Find better way
        }
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Problème de connexion',
          text: 'Identifiants invalides.',
        });
        this.$store.dispatch('failure', this.login);
      });
  }
}

