import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import playerVue from '@/views/templates/_player.component.vue';
import Company from '@/_classes/company.class.ts';
import Pack from '@/_classes/pack.class.ts';
import { mapState, mapGetters, mapActions } from 'vuex';
import store from '@/_store/index';
import storePack from '@/_store/modules/pack';
import storeUser from '@/_store/modules/user';
import storeCompany from '@/_store/modules/company';
import CompanyService from '@/_services/companyService';

const _companyService = new CompanyService();
Vue.use(VueRouter);

@Component({
  components: {
    playerVue,
  },
  name: 'companyContributorsVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
    }),
  },
})
export default class CompanyContributorsVue extends Vue {
  public company: Company = new Company();
  public mounted() {
    this.loadPack();
  }

  get user() {
    return storeUser.state.user;
  }

  public loadPack() {
    const slug = this.$route.params.slug;
    _companyService.getCompany(slug)
      .then((res: any) => {
        const data = res.data;
        this.company = data.company;
      })
      .catch((error: any) => {
        const response = error.response;
        const data = response.data;
        console.log('error', error);
        console.log('data.message', data.message);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la Compagnie',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }
}
