import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import playerVue from '@/views/templates/_player.component.vue';
import cardPackVue from '@/views/Pack/_card_pack.component.vue';
import Company from '@/_classes/company.class.ts';
import Pack from '@/_classes/pack.class.ts';
import { mapState, mapGetters, mapActions } from 'vuex';
import store from '@/_store/index';
import storePack from '@/_store/modules/pack';
import storeUser from '@/_store/modules/user';
import storeCompany from '@/_store/modules/company';
import CompanyService from '@/_services/companyService';

const _companyService = new CompanyService();
Vue.use(VueRouter);

@Component({
  components: {
    playerVue,
    cardPackVue,
  },
  name: 'companyNewVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      packs: (state: any) => state.pack.packs,
      state: (state: any) => state,
    }),
  },
})
export default class CompanyNewVue extends Vue {
  public companies: Company[] = [];
  public company: any = {};
  public selected_pack: Pack = new Pack();
  public first_time = true;

  public mounted() {
    storePack.dispatch('getAllPacks');
  }

  public updated() {
    if (this.first_time) {
      this.checkPackChecked(this.packs);
      this.first_time = false;
    }
  }

  get packs() {
    return storePack.state.packs;
  }

  get user() {
    return storeUser.state.user;
  }

  public checkPackChecked(packs: Pack[]) {
    const urlParams = new URLSearchParams(document.location.search);
    const pack_slug = urlParams.get('pack');

    if (pack_slug) {
      const selected_pack = packs.find((p: Pack) => p.slug === pack_slug);
      this.selected_pack = selected_pack || new Pack();
    }
  }

  public displayPackInformation(pack: Pack) {
    this.selected_pack = pack;
  }

  public createCompany(e: any) {
    e.preventDefault();
    const check: any = _companyService.checkCompanyInfo(this.selected_pack, this.company);
    if (check.value) {
      _companyService.createCompany(check.data)
        .then((res: any) => {
          console.log('res', res);
          storePack.dispatch('getUserCompanies', this.user);
          this.$router.push({name: 'companies'});
        })
        .catch((error: any) => {
          const response = error.response;
          const data = response.data;
          console.log('error', error);
          console.log('data.message', data.message);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Groupe',
            text: 'Un problème c\'est déclarer coté base de donnée',
          });
        });
    } else {
      console.log('check', check);
      this.$notify({
        group: 'return-database',
        type: 'warn',
        duration: -1,
        title: 'Groupe',
        text: 'Le formulaire doit être bien rempli',
      });
    }
  }
}
