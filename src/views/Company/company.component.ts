import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import playerVue from '@/views/templates/_player.component.vue';
import Company from '@/_classes/company.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storeCompany from '@/_store/modules/company';
import moment from 'moment';

Vue.use(VueRouter);

@Component({
  components: {
    playerVue,
  },
  name: 'companyVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      companies: (state: any) => state.company.companies,
      state: (state: any) => state,
    }),
  },
})
export default class CompanyVue extends Vue {
  public mounted() {
    storeCompany.dispatch('getUserCompanies', this.user);
  }

  get companies() {
    return storeCompany.state.companies;
  }

  get user() {
    return storeUser.state.user;
  }

  public displayDateYear(date: any) {
    return moment(date).format('DD/MM/YYYY');
  }
}
