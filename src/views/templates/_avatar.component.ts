import { Component, Prop, Vue } from 'vue-property-decorator';

@Component({
  components: {

  },
  name: 'avatarVue',
})
export default class Avatar extends Vue {
  @Prop() private show_name!: boolean;
  @Prop() private user!: any;
  @Prop() private class_style!: string;
}
