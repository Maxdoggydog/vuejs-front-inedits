import { Component, Prop, Vue } from 'vue-property-decorator';

@Component({
  components: {

  },
  name: 'playerVue',
})
export default class Player extends Vue {
  @Prop() private message!: string;
}
