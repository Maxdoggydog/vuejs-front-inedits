import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
import playerVue from '@/views/templates/_player.component.vue';

@Component({
  components: {
    playerVue,
  },
  name: 'pageVue',
})
export default class Page extends Vue {

}
