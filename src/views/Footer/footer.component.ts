import { Component, Prop, Vue } from 'vue-property-decorator';
import moment from 'moment';

@Component({
  template: require('@/views/Footer/footer.component.vue'),
  components: {

  },
  name: 'FooterVue',
})

export default class Footer extends Vue {
  public dateYear: string  = moment().format('YYYY');

  public links = [
    {
      url: 'cgu',
      name: 'CGU',
    },
  ];
}
