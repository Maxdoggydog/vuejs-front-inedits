import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import playerVue from '@/views/templates/_player.component.vue';
import Selection from '@/_classes/selection.class.ts';
import Post from '@/_classes/post.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storeSelection from '@/_store/modules/selection';
import storePost from '@/_store/modules/post';
import PostService from '@/_services/postService';
import SelectionService from '@/_services/selectionService';
import moment from 'moment';
// @ts-ignore
import draggable from 'vuedraggable'; // Help me to not get the typescript error because file is written in js and not typescript
// import FrontEndService from '@/_services/frontEndService';

// const frontEndService = new FrontEndService();
const _postService = new PostService();
const _selectionService = new SelectionService();
@Component({
  components: {
    playerVue,
    draggable,
  },
  name: 'selectionView',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      selections: (state: any) => state.selection.selections,
      state: (state: any) => state,
    }),
  },
})
export default class SelectionView extends Vue {
  public parent: Post = new Post();
  public mounted() {
    storeSelection.dispatch('getAllSelectionsByUserId', this.user.user.id);
    this.loadParent();
    // frontEndService.initTooltip();
  }

  get user() {
    return storeUser.state.user;
  }

  get selections() {
    return storeSelection.state.selections;
  }

  set selections(selections: any) {
    console.log('après j"enregistre', selections);
    const post_id = this.$route.params.post_id;
    // storeSelection.dispatch('updateSortSelections', {post_id, selections});
  }

  public getCreatedAt(date: string, format: string) {
    return moment(date).format(format);
  }

  public fullName(user: any) {
    return user.first_name + ' ' + user.last_name;
  }

  public loadParent() {
    const post_id = this.$route.params.post_id;
    _postService.getPostById(post_id)
      .then((res: any) => {
        this.parent.setPost(res.data);
        console.log('res', res);
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la Compagnie',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  public sortSelection(eve: any) {
    const startPos: number = eve.oldDraggableIndex;
    const endPos: number = eve.newDraggableIndex;
    const post_id = this.$route.params.post_id;
    _selectionService.updateSortSelections(post_id, this.selections, startPos, endPos)
      .then((res: any) => {
        console.log('res', res);
        this.$notify({
          group: 'return-database',
          type: 'success',
          duration: -1,
          title: 'Trier le tableau',
          text: 'Trie effectué',
        });
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Trier le tableau',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }
}
