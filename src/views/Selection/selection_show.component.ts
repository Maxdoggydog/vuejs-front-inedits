import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import playerVue from '@/views/templates/_player.component.vue';
import Post from '@/_classes/post.class.ts';
import Pdf from '@/_classes/pdf.class.ts';
import Selection from '@/_classes/selection.class.ts';
import { mapState } from 'vuex';
import store from '@/_store';
import storeUser from '@/_store/modules/user';
import storeSelection from '@/_store/modules/selection';
import storePost from '@/_store/modules/post';
import TinyMCEService from '@/_services/tinyMCEService';
import SelectionService from '@/_services/selectionService';
import PdfService from '@/_services/pdfService';
import Editor from '@tinymce/tinymce-vue';

const _tinyMCEService = new TinyMCEService();
const _selectionService = new SelectionService();
const _pdfService = new PdfService();
@Component({
  components: {
    playerVue,
    editor: Editor,
  },
  name: 'selectionShowView',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      selections: (state: any) => state.selection.selections,
      state: (state: any) => state,
    }),
  },
})
export default class SelectionShowView extends Vue {
  public selection: Selection = new Selection();
  public pdf: Pdf = new Pdf();
  public config: object = _tinyMCEService.config;
  public api_key: string = _tinyMCEService.api_key;
  public first_time = true;

  public mounted() {
    storeSelection.dispatch('getAllSelectionsByUserId', this.user.user.id);
    storePost.dispatch('getPostById', this.$route.params.post_id);
  }

  public updated() {
    if (this.first_time) {
      if (!this.$route.params.pdf_id) {
        this.load();
      } else {
        this.editPdf();
      }
      this.first_time = false;
    }
  }

  get user() {
    return storeUser.state.user;
  }

  get selections() {
    return storeSelection.state.selections;
  }

  get parent() {
    return storePost.state.post;
  }

  /**
   * Load the text for the pdf
   */
  public editPdf() {
    const pdf_id = this.$route.params.pdf_id;
    _pdfService.getPdfById(pdf_id)
      .then((res: any) => {
        this.pdf = res.data;
      })
      .catch((error: any) => {
        const response = error.response;
        const data = response.data;
        console.log('error', error);
        console.log('data.message', data.message);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la sélection',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  /**
   * Load the text for the pdf
   */
  public load() {
    const user_id = this.user.user.id;
    _selectionService.getSelectedPost(user_id, this.parent.id, this.parent.slug)
      .then((res: any) => {
        const data = res.data;
        this.pdf = data.pdf;
      })
      .catch((error: any) => {
        const response = error.response;
        const data = response.data;
        console.log('error', error);
        console.log('data.message', data.message);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur la sélection',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  /**
   * Create the pdf
   */
  public createPDF(e: any) {
    e.preventDefault();
    const check = _pdfService.checkPostInfo(this.pdf);

    if (check.value) {
      _pdfService.createPdf(this.pdf, this.$route.params.post_id)
        .then((res: any) => {
          console.log('res', res);
          const data: any = res.data;

          this.$router.push({name: 'pdfs'});
          this.$notify({
            group: 'return-database',
            type: 'success',
            duration: -1,
            title: 'PDF créé',
            text: 'Le PDF a bien été créé',
          });

        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Problème création pdf',
            text: 'Backend',
          });
        });
    } else {
      this.$notify({
        group: 'return-database',
        type: 'error',
        duration: -1,
        title: 'Problème création pdf',
        text: 'Le PDF doit être bien rempli',
      });
    }

  }
}
