import { Component, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';

Vue.use(VueRouter);
@Component({
  components: {

  },
})
export default class RegisterCheckEmail extends Vue {
  public email: string = this.$route.params.email;

  public mounted() {
    this.checkEmail(this.email);
  }

  public checkEmail(email: string) {
    if (email) {
      this.$notify({
        group: 'return-database',
        type: 'success',
        duration: -1,
        title: 'Utilisateur créé',
        text: 'Un utilisateur a été créé',
      });
    } else {
      this.$router.push({ name: 'home' });
    }
  }
}
