import { Component, Vue } from 'vue-property-decorator';
import playerVue from '@/views/templates/_player.component.vue';
import VueRouter from 'vue-router';
import RegisterService from '@/_services/registerService';

const _registerService = new RegisterService();

Vue.use(VueRouter);
@Component({
  components: {
    playerVue,
  },
})

export default class Register extends Vue {
  public cle_site: string = process.env.VUE_APP_CLE_SITE_RECAPTCHA;
  public user: any = {avatars: '', covers: ''};
  public formData = new FormData();

  public checkForm(e: any) {
    e.preventDefault();
    const recaptcha: any = document.getElementsByClassName('g-recaptcha-response');
    const value = 'value';
    this.user.recaptcha = recaptcha[0][value];

    const check: any = _registerService.createUserProfile(this.user);
    console.log('user', this.user);

    if (check.value) {
      _registerService.getRegister(this.user)
        .then((res: any) => {
          console.log('res', res);
          this.$router.push({ name: 'register-check-email', params: {email: this.user.email }});
        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Utilisateur non créé',
            text: 'Un problème c\'est déclarer coté base de donnée',
          });
        });
    } else {
      console.log('check', check);
      this.$notify({
        group: 'return-database',
        type: 'warn',
        duration: -1,
        title: 'Formulaire',
        text: 'Le formulaire doit être bien rempli',
      });
    }
  }

  public uploadFile(event: any, field: string) {
    _registerService.fileUpload(event, field)
      .then((res: any) => {
        if (field === 'avatars') {
          this.user.avatars = res.path;
        } else {
          this.user.covers = res.path;
        }

        console.log('res', res);
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Upload photo',
          text: 'La photo n\'a pu être uploadé',
        });
      });

  }
}
