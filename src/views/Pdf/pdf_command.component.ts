import { Component, Prop, Vue } from 'vue-property-decorator';
import { mapState } from 'vuex';
import Pdf from '@/_classes/pdf.class.ts';
import store from '@/_store/index';
import storeUser from '@/_store/modules/user';
import storePdf from '@/_store/modules/pdf';
import PdfService from '@/_services/pdfService';
import playerVue from '@/views/templates/_player.component.vue';

const _pdfService = new PdfService();

@Component({
  components: {
    playerVue,
  },
  name: 'pdfCommandVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      pdfs: (state: any) => state.pdf.pdfs,
      state: (state: any) => state,
    }),
  },

})

export default class PdfCommandVue extends Vue {
  public pdf: Pdf = new Pdf();
  public mounted() {
    this.load();
  }

  get user() {
    return storeUser.state.user;
  }

  get pdfs() {
    return storePdf.state.pdfs;
  }

  public load() {
    const post_id = this.$route.params.post_id;
    _pdfService.getPdfById(post_id)
      .then((res: any) => {
        const data = res.data;
        this.pdf = data.pdf;
      })
      .catch((error: any) => {
        const response = error.response;
        const data = response.data;
        console.log('error', error);
        console.log('data.message', data.message);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Charger info sur le Pdf',
          text: 'Un problème c\'est déclarer coté base de donnée',
        });
      });
  }

  public toCommand(e: any) {
    e.preventDefault();
  }

}
