import { Component, Prop, Vue } from 'vue-property-decorator';
import { mapState } from 'vuex';
import Pdf from '@/_classes/pdf.class.ts';
import store from '@/_store/index';
import storeUser from '@/_store/modules/user';
import storePdf from '@/_store/modules/pdf';
import PdfService from '@/_services/pdfService';
import playerVue from '@/views/templates/_player.component.vue';

const _pdfService = new PdfService();

@Component({
  components: {
    playerVue,
  },
  name: 'pdfsVue',
  computed: {
    ...mapState({
      user: (state: any) => state.user.user,
      pdfs: (state: any) => state.pdf.pdfs,
      state: (state: any) => state,
    }),
  },

})

export default class PdfsVue extends Vue {
  public selectedPdf: Pdf = new Pdf();
  public mounted() {
    this.selectedPdf = new Pdf();
    storePdf.dispatch('getAllPdfsByUserId', this.user.user.id);
  }

  // public updated() {
  //   console.log('this.pdfs', this.pdfs);
  // }

  get user() {
    return storeUser.state.user;
  }

  get pdfs() {
    return storePdf.state.pdfs;
  }

  /**
   * Select pdf to delete
   */
  public selectPdf(pdf: Pdf) {
    this.selectedPdf = pdf;
  }

  /**
   * Select pdf to delete
   */
  public unSelectPdf(pdf: Pdf) {
    this.selectedPdf = new Pdf();
  }

  /**
   * Delete pdf
   */
  public deletePdf(pdf: Pdf) {
    if (pdf.id !== 0) {
      const pdf_id = pdf.id;
      _pdfService.deletePdfBySlug(pdf)
        .then((res: any) => {
          console.log('res', res);
          const pos = this.pdfs.findIndex((p: Pdf) => p.id === pdf.id );
          this.pdfs.splice(pos, 1);
          const htmlElement: any = $('#deletePdfModal');
          htmlElement.modal('hide');
          this.selectedPdf = new Pdf();

          this.$notify({
            group: 'return-database',
            type: 'success',
            duration: -1,
            title: 'Pdf supprimer',
            text: 'Le pdf a été supprimé',
          });

        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Problème supprimer pdf',
            text: 'Backend',
          });
        });
    } else {
      this.$notify({
        group: 'return-database',
        type: 'warning',
        duration: -1,
        title: 'Problème supprimer pdf',
        text: 'Sélectionnner un pdf à supprimer',
      });
    }
  }

  /**
   * Show pdf
   */
  public showPDF(pdf: Pdf) {
    _pdfService.getPdfById(pdf.id)
      .then((res: any) => {
        console.log('res', res);

      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Problème montrer pdf',
          text: 'Backend',
        });
      });
  }

}
