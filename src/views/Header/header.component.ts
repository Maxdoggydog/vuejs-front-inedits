import { Component, Prop, Vue } from 'vue-property-decorator';
import avatarVue from '@/views/templates/_avatar.component.vue';
import { mapState } from 'vuex';
import store from '@/_store/index';
import storeUser from '@/_store/modules/user';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

@Component({
  template: require('@/views/Header/header.component.vue'),
  components: {
    avatarVue,
  },
  name: 'HeaderVue',
  computed: mapState({
    userState: 'user',
  }),

})

export default class Header extends Vue {
  public user: any = {};
  public menus = [
    {
      name: 'Accueil',
      show: true,
      url: 'home',
    },
    {
      name: 'Comment ça marche',
      show: true,
      url: 'comment-ca-marche',
    },
    {
      name: 'Les auteurs',
      show: this.user.id,
      url: 'authors',
    },
    {
      name: 'Les arbres',
      show: this.user.id,
      url: 'trees',
    },
    {
      name: 'Mes livres',
      show: this.user.id,
      url: 'pdfs',
    },
    {
      name: 'Mes groupes',
      show: this.user.id,
      url: 'companies',
    },
    {
      name: 'Nos prestations',
      show: true,
      url: 'prestations',
    },
  ];

  public mounted() {
    this.user = storeUser.state.user;
  }

  public updated() {
    // this.user = store.state.user;
  }

  // get userDisplay() {
  //   return user;
  // }

  public logOut() {
    _logInService.getLogOut()
      .then((res: any) => {
        console.log('res', res);
        localStorage.removeItem('user');
        this.user = {};
        this.$router.push('/');
        storeUser.dispatch('logout');
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Problème de connexion',
          text: 'Identifiants invalides.',
        });
        storeUser.dispatch('failure');
      });
  }
}
