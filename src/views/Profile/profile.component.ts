import { Component, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import coverVue from '@/views/templates/_cover.component.vue';
import avatarVue from '@/views/templates/_avatar.component.vue';
import postBoxVue from '@/views/Post/_post_box.component.vue';
import User from '@/_classes/user.class.ts';
import store from '@/_store/index';
import storeUser from '@/_store/modules/user';

Vue.use(VueRouter);
@Component({
  components: {
    coverVue,
    avatarVue,
    postBoxVue,
  },
})
export default class Profile extends Vue {
  public user: User = new User();

  public mounted() {
    this.user = storeUser.state.user;
  }
}
