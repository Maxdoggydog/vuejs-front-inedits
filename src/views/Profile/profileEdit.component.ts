import { Component, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import coverVue from '@/views/templates/_cover.component.vue';
import avatarVue from '@/views/templates/_avatar.component.vue';
import User from '@/_classes/user.class.ts';
import store from '@/_store/index';
import storeUser from '@/_store/modules/user';
import RegisterService from '@/_services/registerService';

const _registerService = new RegisterService();

Vue.use(VueRouter);
@Component({
  components: {
    coverVue,
    avatarVue,
  },
})
export default class ProfileEdit extends Vue {
  public user: User = new User();
  public password: any = {};

  public mounted() {
    this.user = storeUser.state.user;
  }

  public changeInformation(e: any) {
    e.preventDefault();

    const check: any = _registerService.checkUserProfileToUpdate(this.user);

    if (check.value) {
      _registerService.updateUserProfile(this.user)
        .then((res: any) => {
          console.log('res', res);
          this.$router.push({ name: 'profile', params: {slug: this.user.slug }});
        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Information non changé',
            text: 'Un problème c\'est déclarer coté base de donnée',
          });
        });
    } else {
      console.log('check', check);
      this.$notify({
        group: 'return-database',
        type: 'warn',
        duration: -1,
        title: 'Formulaire',
        text: 'Le formulaire doit être bien rempli',
      });
    }
  }

  public changePassword(e: any) {
    e.preventDefault();

    const check: any = _registerService.checkUserPasswordToUpdate(this.user);

    if (check.value) {
      _registerService.updateUserPassword(this.user, this.password)
        .then((res: any) => {
          console.log('res', res);
          this.$router.push({ name: 'profile', params: {slug: this.user.slug }});
        })
        .catch((error: any) => {
          console.log('error', error);
          this.$notify({
            group: 'return-database',
            type: 'error',
            duration: -1,
            title: 'Information non changé',
            text: 'Un problème c\'est déclarer coté base de donnée',
          });
        });
    } else {
      console.log('check', check);
      this.$notify({
        group: 'return-database',
        type: 'warn',
        duration: -1,
        title: 'Formulaire',
        text: 'Le formulaire doit être bien rempli',
      });
    }
  }

  public uploadFile(event: any, field: string) {
    _registerService.fileUpload(event, field)
      .then((res: any) => {
        if (field === 'avatars') {
          this.user.avatars = res.path;
        } else {
          this.user.covers = res.path;
        }

        console.log('res', res);
      })
      .catch((error: any) => {
        console.log('error', error);
        this.$notify({
          group: 'return-database',
          type: 'error',
          duration: -1,
          title: 'Upload photo',
          text: 'La photo n\'a pu être uploadé',
        });
      });

  }

}
