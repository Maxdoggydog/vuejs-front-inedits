import { Component, Prop, Vue } from 'vue-property-decorator';
import Pack from '@/_classes/pack.class.ts';

@Component({
  components: {

  },
  name: 'cardPackMoreVue',
})
export default class CardPackMoreVue extends Vue {
  @Prop() private user!: object;
  @Prop() private pack!: Pack;
}
