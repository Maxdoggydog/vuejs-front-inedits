import { Component, Prop, Vue } from 'vue-property-decorator';
import VueRouter from 'vue-router';
import { mapState, mapGetters, mapActions } from 'vuex';
import store from '@/_store/index';
import storePack from '@/_store/modules/pack';
import storeUser from '@/_store/modules/user';
import Pack from '@/_classes/pack.class.ts';

Vue.use(VueRouter);
import playerVue from '@/views/templates/_player.component.vue';
import cardPackVue from '@/views/Pack/_card_pack.component.vue';

@Component({
  components: {
    playerVue,
    cardPackVue,
  },
  name: 'packVue',
  computed: {
    ...mapState({
      packs: (state: any) => state.pack.packs,
      // user: (state) => state.user.user,
      state: (state: any) => state,
    }),
    // ...mapGetters({
    //   packs: (getter: any) => { console.log('getter', getter); },
    // }),
  },
  // methods: mapActions(['stores/modules/pack/getAllPacks']),
  methods: mapActions({
    getAllPacks: 'pack/getAllPacks',
  }),
})
export default class PackVue extends Vue {
  public contact_us: string = process.env.VUE_APP_CONTACT_US;
  public host: string = process.env.VUE_APP_API_DOMAINE;

  public mounted() {
    // console.log('packs', this.packs);
    // console.log('state', this.state);
    console.log('this', this);
    // console.log('this.getAllPacks', this.getAllPacks());
    console.log('storePack', storePack);
    storePack.dispatch('getAllPacks');
  }

  get packs() {
    return storePack.state.packs;
  }

  get user() {
    return storeUser.state.user;
  }
}
