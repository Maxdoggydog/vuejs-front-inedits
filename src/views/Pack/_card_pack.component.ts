import { Component, Prop, Vue } from 'vue-property-decorator';
import cardPackMoreVue from '@/views/Pack/_pack_more_infos.component.vue';
import Pack from '@/_classes/pack.class.ts';
import User from '@/_classes/user.class.ts';

@Component({
  components: {
    cardPackMoreVue,
  },
  name: 'cardPackVue',
})
export default class CardPackVue extends Vue {
  public host: string = process.env.VUE_APP_API_DOMAINE;
  public default_path: string =  this.host + '/pro/statics/images/sur_mesure.jpg';
  @Prop() private create!: boolean;
  @Prop() private selected!: boolean;
  @Prop() private user!: User;
  @Prop() private pack!: Pack;

}
