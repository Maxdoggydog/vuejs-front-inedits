import Vue from 'vue';
// import VueD3 from 'vue2-d3';
// import BootstrapVue from 'bootstrap-vue';
import './assets/scss/custom.scss';
// // Font Awesome scss | classic
// import './assets/font/fontawesome/scss/fontawesome.scss';
// import './assets/font/fontawesome/scss/brands.scss';
// import './assets/font/fontawesome/scss/solid.scss';
// // Font Awesome js | classic
// import './assets/font/fontawesome/js/fontawesome.js';
// import './assets/font/fontawesome/js/brands.js';
// import './assets/font/fontawesome/js/solid.js';
// Font with Vue
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faAt,
  faLock,
  faDownload,
  faCaretDown,
  faUser,
  faEnvelope,
  faUnlockAlt,
  faArrowRight,
  faArrowLeft,
  faCloudDownloadAlt,
  faCloudUploadAlt,
  faEdit,
  faCheck,
  faEye,
  faPencilAlt,
  faTrash,
  faQuestionCircle,
  faEraser,
  faArrowsAlt,
  faAngleDoubleRight,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome';

import App from './App/app.component.vue';
import router from './router';
import store from './_store/index';
import './_services/registerServiceWorker';
import InitMeta from './_services/initMetaData';
import VueResource from 'vue-resource';
import Notifications from 'vue-notification';


library.add(
  faAt,
  faLock,
  faDownload,
  faCaretDown,
  faUser,
  faEnvelope,
  faUnlockAlt,
  faArrowRight,
  faArrowLeft,
  faCloudDownloadAlt,
  faCloudUploadAlt,
  faEdit,
  faCheck,
  faEye,
  faPencilAlt,
  faTrash,
  faQuestionCircle,
  faEraser,
  faArrowsAlt,
  faAngleDoubleRight,
);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('font-awesome-layers', FontAwesomeLayers);

Vue.config.productionTip = false;
// Vue.use(BootstrapVue);
Vue.use(VueResource, FontAwesomeIcon);
Vue.use(Notifications);
// Vue.use(VueD3);

const initMetaService = new InitMeta();
initMetaService.initMeta(router);
initMetaService.checkLoggedUser(router);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
