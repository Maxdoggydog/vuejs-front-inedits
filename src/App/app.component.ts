import { Component, Prop, Vue } from 'vue-property-decorator';
import FooterVue from '@/views/Footer/footer.component.vue';
import HeaderVue from '@/views/Header/header.component.vue';

@Component({
  template: require('@/App/app.component.vue'),
  components: {
    FooterVue,
    HeaderVue,
  },
})

export default class App extends Vue {

}
