import Vue from 'vue';
import Vuex from 'vuex';

import user from '@/_store/modules/user';
import pack from '@/_store/modules/pack';
import post from '@/_store/modules/post';
import pdf from '@/_store/modules/pdf';
import selection from '@/_store/modules/selection';
import company from '@/_store/modules/company';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    pack,
    post,
    pdf,
    selection,
    company,
  },
});
