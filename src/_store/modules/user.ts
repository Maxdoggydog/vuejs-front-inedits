import Vue from 'vue';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import router from '@/router';
import User from '@/_classes/user.class.ts';

import LogInService from '@/_services/logInService';

const new_user = new User();
const _logInService = new LogInService();
const user: User = new_user.setProfile(JSON.parse(localStorage.getItem('user') || '{}'));
const initialState = user ? { status: { loggedIn: true }, user } : { status: {}, user: new_user };

Vue.use(Vuex);

export default new Vuex.Store({
  state: initialState,
  actions: {
    login({ dispatch, commit }, {u}) {
      commit('loginRequest', u);
      commit('loginSuccess', u);
    },
    logout({ dispatch, commit }) {
      commit('loginOut');
    },
    failure({ dispatch, commit }) {
      commit('loginFailure');
    },
  },
  mutations: {
    loginRequest(state: any, u: User) {
      state.status = { loggingIn: true };
      state.user = u;
    },
    loginSuccess(state: any, u: User) {
      state.status = { loggedIn: true };
      state.user = u;
    },
    loginFailure(state: any) {
      state.status = new_user;
      state.user = null;
    },
    loginOut(state: any) {
      state.status = { loggedIn: false };
      state.user = new_user;
    },
  },
});
