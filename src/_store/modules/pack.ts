import Vue from 'vue';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import router from '@/router';
import Pack from '@/_classes/pack.class.ts';

import PackService from '@/_services/packService';

const _packService = new PackService();
const packs: Pack[] = [];

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: {

    },
    packs,
  },
  actions: {
    getAllPacks: async ({commit, dispatch}) => {
      const ps = await _packService.getAllPacks();
      const data = ps.data;
      const packsArray = data.map((d: any) => {
        const new_pack = new Pack();
        new_pack.setPack(d);
        return new_pack;
      });
      commit('loadPacks', packsArray);
    },
  },
  mutations: {
    loadPacks(state: any, ps: Pack[]) {
      state.status = { loaded: true };
      state.packs = ps;
    },
  },
});
