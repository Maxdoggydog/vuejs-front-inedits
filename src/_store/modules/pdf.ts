import Vue from 'vue';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import router from '@/router';
import Pdf from '@/_classes/pdf.class.ts';

import PdfService from '@/_services/pdfService';

const _pdfService = new PdfService();
const pdfs: Pdf[] = [];

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: {

    },
    pdfs,
  },
  actions: {
    getAllPdfs: async ({commit, dispatch}) => {
      const ps = await _pdfService.getAllPdfs();
      const data = ps.data;
      const pdfsArray = data.map((d: any) => {
        const new_pdf = new Pdf();
        new_pdf.setPdf(d);
        return new_pdf;
      });
      commit('loadPacks', pdfsArray);
    },
    getAllPdfsByUserId: async ({commit, dispatch}, user_id) => {
      const ps = await _pdfService.getAllPdfsByUserId(user_id);
      const data = ps.data;
      const pdfsArray = data.map((d: any) => {
        const new_pdf = new Pdf();
        new_pdf.setPdf(d);
        return new_pdf;
      });
      commit('loadPacks', pdfsArray);
    },
  },
  mutations: {
    loadPacks(state: any, ps: Pdf[]) {
      state.status = { loaded: true };
      state.pdfs = ps;
    },
  },
});
