import Vue from 'vue';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import router from '@/router';
import Selection from '@/_classes/selection.class.ts';

import SelectionService from '@/_services/selectionService';

const _selectionService = new SelectionService();
const selections: Selection[] = [];

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: {

    },
    selections,
  },
  actions: {
    getAllSelections: async ({commit, dispatch}) => {
      const ps = await _selectionService.getAllSelections();
      const data = ps.data;
      const selectionsArray = data.map((d: any) => {
        const new_selection = new Selection();
        new_selection.setSelection(d);
        return new_selection;
      });
      commit('loadPacks', selectionsArray);
    },
    getAllSelectionsByUserId: async ({commit, dispatch}, user_id) => {
      const ps = await _selectionService.getAllSelectionsByUserId(user_id);
      const data = ps.data;
      const selectionsArray = data.map((d: any) => {
        const new_selection = new Selection();
        new_selection.setSelection(d);
        return new_selection;
      });
      commit('loadPacks', selectionsArray);
    },
    // updateSortSelections: ({commit, dispatch}, obj) => {
    //   _selectionService.updateSortSelections(obj.post_id, obj.selectionsUpdated)
    //     .then((res: any) => {
    //       console.log('res', res);
    //       commit('successUpdateSelection');
    //     })
    //     .catch((error: any) => {
    //       console.log('error', error);
    //       commit('failUpdateSelection');
    //     });
    // },
  },
  mutations: {
    loadPacks(state: any, ps: Selection[]) {
      state.status = { loaded: true };
      state.selections = ps;
    },
    successUpdateSelection(state: any) {
      state.status = { updated: true };
    },
    failUpdateSelection(state: any) {
      state.status = { updated: false };
    },
  },
});
