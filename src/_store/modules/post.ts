import Vue from 'vue';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import router from '@/router';
import Post from '@/_classes/post.class.ts';

import PostService from '@/_services/postService';

const _postService = new PostService();
const posts: Post[] = [];
const post: Post = new Post();

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: {

    },
    posts,
    post,
  },
  actions: {
    getAllPosts: async ({commit, dispatch}) => {
      const ps = await _postService.getAllPosts();
      const data = ps.data;
      const postsArray = data.map((d: any) => {
        const new_post = new Post();
        new_post.setPost(d);
        return new_post;
      });
      commit('loadPosts', postsArray);
    },
    getAllPostsByCompany: async ({commit, dispatch}, company_id) => {
      const ps = await _postService.getAllPostsByCompany(company_id);
      const data = ps.data;
      const postsArray = data.map((d: any) => {
        const new_post = new Post();
        new_post.setPost(d);
        return new_post;
      });
      commit('loadPosts', postsArray);
    },
    getRootPost: async ({commit, dispatch}) => {
      const ps = await _postService.getRootPost();
      const data = ps.data;
      const postsArray = data.map((d: any) => {
        const new_post = new Post();
        new_post.setPost(d);
        return new_post;
      });
      commit('loadPosts', postsArray);
    },
    getRootPostByUser: async ({commit, dispatch}, user) => {
      const ps = await _postService.getRootPostByUser(user.user.id);
      const data = ps.data;
      const postsArray = data.map((d: any) => {
        const new_post = new Post();
        new_post.setPost(d);
        return new_post;
      });
      commit('loadPosts', postsArray);
    },
    getPostById: async ({commit, dispatch}, post_id) => {
      const p = await _postService.getPostById(post_id);
      const data: Post = new Post();
      data.setPost(p.data);
      commit('loadPost', data);
    },
  },
  mutations: {
    loadPosts(state: any, ps: Post[]) {
      state.status = { loaded: true };
      state.posts = ps;
    },
    loadPost(state: any, p: Post) {
      state.status = { loaded: true };
      state.post = p;
    },
  },
});
