import Vue from 'vue';
import Vuex from 'vuex';
import { mapState } from 'vuex';
import router from '@/router';
import Company from '@/_classes/company.class.ts';

import CompanyService from '@/_services/companyService';

const _companyService = new CompanyService();
const companies: Company[] = [];

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: {

    },
    companies,
  },
  actions: {
    getUserCompanies: async ({commit, dispatch}, user) => {
      const ps = await _companyService.getUserCompanies(user.user.id);
      const data = ps.data;
      const companiesArray = data.map((d: any) => {
        const new_company = new Company();
        new_company.setCompany(d);
        return new_company;
      });
      commit('loadCompanies', companiesArray);
    },
  },
  mutations: {
    loadCompanies(state: any, cp: Company[]) {
      state.status = { loaded: true };
      state.companies = cp;
    },
  },
});
