import axios from 'axios';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

export default class RegisterService {
  private min_length_firstname = 3;
  private min_length_lastname = 6;
  private min_length_email = 8;
  private min_length_password = 8;

  private options: object = {headers : _logInService.headers};

  /**
   * Create user
   */
  public getRegister(user_params: object) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/register/register', user_params, this.options);
  }

  /**
   * Upload file for user
   */
  public upload(file: object) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/register/upload/picture', file, this.options);
  }

  /**
   * Check if create a userprofile is possible
   */
  public createUserProfile(userprofile_data: any) {
    const data = userprofile_data;
    return this.checkUserProfile(data);
  }

  /**
   * Check if create a userprofile is possible
   */
  public checkUserProfile(userprofile_params: any) {
    const checkFirstname = this.checkFirstname(userprofile_params.firstname);
    if (checkFirstname.value === false) {
      return checkFirstname;
    }

    const checkLastname = this.checkLastname(userprofile_params.lastname);
    if (checkLastname.value === false) {
      return checkLastname;
    }

    const checkEmail = this.checkEmail(userprofile_params.email);
    if (checkEmail.value === false) {
      return checkEmail;
    }

    const checkPassword = this.checkPassword(userprofile_params.password);
    if (checkPassword.value === false) {
      return checkPassword;
    }

    const checkPasswordConfirm = this.checkPasswordConfirm(userprofile_params.password, userprofile_params.confirm_password);
    if (checkPasswordConfirm.value === false) {
      return checkPasswordConfirm;
    }

    const checkCgu = this.checkCgu(userprofile_params.cgu);
    if (checkCgu.value === false) {
      return checkCgu;
    }

    // const checkRecaptcha = this.checkRecaptcha('recaptcha-anchor');
    // if (checkRecaptcha.value === false) {
    //   return checkRecaptcha;
    // }

    return {value: true, data: userprofile_params, code: ''};
  }

  /**
   * Check for the firstname
   */
  public checkFirstname(firstname: string) {
    if (!firstname) {
      return {value: false, message: 'Firstname is Null', code: 'firstname_is_null'};
    } else if (firstname === '') {
      return {value: false, message: 'Firstname is empty', code: 'firstname_is_empty'};
    } else if (firstname.length < this.min_length_firstname) {
      return {value: false, message: 'Firstname length is too short, ' + this.min_length_firstname + ' is needed for ' + firstname, code: 'firstname_too_short'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for lastname
   */
  public checkLastname(lastname: string) {
    if (!lastname) {
      return {value: false, message: 'Lastname is Null', code: 'lastname_is_null'};
    } else if (lastname === '') {
      return {value: false, message: 'Lastname is empty', code: 'lastname_is_empty'};
    } else if (lastname.length < this.min_length_lastname) {
      return {value: false, message: 'Lastname length is too short, ' + this.min_length_lastname + ' is needed', code: 'lastname_too_short'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for email
   */
  public checkEmail(email: string) {
    if (!email) {
      return {value: false, message: 'Email is Null', code: 'email_is_null'};
    } else if (email === '') {
      return {value: false, message: 'Email is empty', code: 'email_is_empty'};
    } else if (email.length < this.min_length_email) {
      return {value: false, message: 'Lastname length is too short, ' + this.min_length_email + ' is needed', code: 'email_too_short'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for password
   */
  public checkPassword(password: string) {
    const r = /\d+/ /* number */;
    const r2 = /[A-Za-z]/ /* string */;
    if (!password) {
      return {value: false, message: 'Password is Null', code: 'email_is_null'};
    } else if (password === '') {
      return {value: false, message: 'Password is empty', code: 'email_is_empty'};
    } else if (password.length < this.min_length_password) {
      return {value: false, message: 'Password length is too short, ' + this.min_length_password + ' is needed', code: 'email_too_short'};
    } else if (!password.match(r2)) {
      return {value: false, message: 'The password must contain at least one letter', code: 'email_complexity'};
    } else if (!password.match(r)) {
      return {value: false, message: 'The password must contain at least one digit or punctuation character', code: 'email_complexity'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for password
   */
  public checkPasswordConfirm(password: string, confirm_password: string) {
    if (confirm_password !== password) {
      return {value: false, message: 'The confirm password is not the same', code: 'confirm_password_not_same'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check cgu checked
   */
  public checkCgu(cgu: boolean) {
    if (cgu !== true) {
      return {value: false, message: 'The cgu is not checked', code: 'cgu_not_confirmed'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check for the recaptcha
   */
  public checkRecaptcha(selector: string) {
    const recaptchaDiv = document.getElementById(selector);
    if (recaptchaDiv) {
      const value = 'value';
      const aria: any = 'aria-checked';
      const attributes = recaptchaDiv.attributes[aria];
      const response = attributes[value];

      if (response === 'true') {
        return {value: true, message: '', code: ''};
      } else {
        return {value: false, message: 'Captcha not checked', code: 'recaptcha_is_not_checked'};
      }
    } else {
      return {value: false, message: 'Need to click on the captcha', code: 'recaptcha_need_to_click'};
    }
  }

  /**
   * Upload file 'Picture'
   */
  public fileUpload(event: any, fieldname: string) {
    const formData = new FormData();
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];
      formData.append('file', file);
      formData.append('filename', file.name);
      formData.append('folder', fieldname);
    }

    return this.upload(formData);
  }

  /**
   * Check if create a userprofile is possible
   */
  public checkUserProfileToUpdate(user: any) {
    const checkFirstname = this.checkFirstname(user.firstname);
    if (checkFirstname.value === false) {
      return checkFirstname;
    }

    const checkLastname = this.checkLastname(user.lastname);
    if (checkLastname.value === false) {
      return checkLastname;
    }

    const checkEmail = this.checkEmail(user.email);
    if (checkEmail.value === false) {
      return checkEmail;
    }

    return {value: true, data: user, code: ''};
  }

  /**
   * Check if create a userprofile is possible
   */
  public checkUserPasswordToUpdate(user: any) {
    const checkPassword = this.checkPassword(user.password);
    if (checkPassword.value === false) {
      return checkPassword;
    }

    const checkPasswordConfirm = this.checkPasswordConfirm(user.password, user.confirm_password);
    if (checkPasswordConfirm.value === false) {
      return checkPasswordConfirm;
    }

    return {value: true, data: user, code: ''};
  }

  /**
   * Change user profile
   */
  public updateUserProfile(user: any) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/userprofile/update', user, this.options);
  }

  /**
   * Change user password
   */
  public updateUserPassword(user: any, password: any) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/userprofile/password/update', {user, password}, this.options);
  }
}
