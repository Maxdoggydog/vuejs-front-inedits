import axios from 'axios';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

export default class PdfService {
  private options: object = {headers : _logInService.headers};

  /**
   * Auth for header
   */
  public authHeader() {
    // return authorization header with jwt token
    const user = JSON.parse(localStorage.getItem('user') || '{}');

    if (user && user.token) {
      return { Authorization: 'Bearer ' + user.token };
    } else {
      return {};
    }
  }

  /**
   * Get all Posts
   */
  public getAllPdfs() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/all', {});
  }

  /**
   * Get all Posts by company
   */
  public getAllPdfsByUserId(user_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/user/' + user_id, this.options);
  }

  /**
   * Get all Posts by pdf_id
   */
  public getPdfById(pdf_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/' + pdf_id, this.options);
  }

  /**
   * Get all Posts by slug
   */
  public getPdfBySlug(slug: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/' + slug, {});
  }

  /**
   * Create pdf
   */
  public createPdf(pdf: any, post_id: any) {
    const api_url = !pdf.id ? process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/create' : process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/edit';
    pdf.post_id = post_id;
    return axios.post(api_url, pdf, this.options);
  }

  /**
   * Delete pdf by id
   */
  public deletePdfBySlug(pdf: any) {
    return axios.delete(process.env.VUE_APP_API_DOMAINE + '/pro/pdfs/' + pdf.slug + '/delete/' + pdf.id, this.options);
  }

  /**
   * Check value for Post title
   */
  public checkTitle(title: string) {
    if (!title) {
      return {value: false, message: 'Title is null', code: 'title_is_null'};
    } else if (title === '') {
      return {value: false, message: 'Title is empty', code: 'title_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }



  /**
   * Post check
   */
  public checkPostInfo(pdf: any) {
    const checkTitle = this.checkTitle(pdf.title);
    if (checkTitle.value === false) {
      return checkTitle;
    }

    return {value: true, data: pdf, code: ''};
  }

}
