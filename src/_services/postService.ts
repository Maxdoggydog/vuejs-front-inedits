import axios from 'axios';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

export default class PostService {
  private options: object = {headers : _logInService.headers};

  /**
   * Get all Posts
   */
  public getAllPosts() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/all', this.options);
  }

  /**
   * Get all Posts by company
   */
  public getAllPostsByCompany(company_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/company/' + company_id, this.options);
  }

  /**
   * Get all Posts by post_id
   */
  public getPostById(post_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/' + post_id, this.options);
  }

  /**
   * Get the parent/root by post_id
   */
  public getParent(post_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/' + post_id + '/parent', this.options);
  }

  /**
   * Get all Posts by slug
   */
  public getPostBySlug(slug: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/' + slug, this.options);
  }


  /**
   * Get all Posts root
   */
  public getRootPost() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/roots', this.options);
  }

  /**
   * Get all Posts root by user
   */
  public getRootPostByUser(user_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/posts/roots/user/' + user_id, this.options);
  }

  /**
   * Create post
   */
  public createPost(post: object, company_id: any, parent_id?: any) {
    const api_url = parent_id ? process.env.VUE_APP_API_DOMAINE + '/pro/posts/new/company/' + company_id + '/' + parent_id : process.env.VUE_APP_API_DOMAINE + '/pro/posts/new/company/' + company_id;
    return axios.post(api_url, post, this.options);
  }

  /**
   * Edit post
   */
  public editPost(post: any) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/posts/' + post.id + '/edit', post, this.options);
  }

  /**
   * Check value for Post title
   */
  public checkTitle(title: string) {
    if (!title) {
      return {value: false, message: 'Title is null', code: 'title_is_null'};
    } else if (title === '') {
      return {value: false, message: 'Title is empty', code: 'title_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for Post title
   */
  public checkLastName(last_name: string) {
    if (!last_name) {
      return {value: false, message: 'Last name is null', code: 'last_name_is_null'};
    } else if (last_name === '') {
      return {value: false, message: 'Last name is empty', code: 'last_name_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for Post title
   */
  public checkFirstName(first_name: string) {
    if (!first_name) {
      return {value: false, message: 'First name is null', code: 'first_name_is_null'};
    } else if (first_name === '') {
      return {value: false, message: 'First name is empty', code: 'first_name_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Post check
   */
  public checkPostInfo(post: any) {
    const checkTitle = this.checkTitle(post.title);
    if (checkTitle.value === false) {
      return checkTitle;
    }
    const checkLastName = this.checkLastName(post.user_profile.user.last_name);
    if (checkLastName.value === false) {
      return checkLastName;
    }
    const checkFirstName = this.checkFirstName(post.user_profile.user.first_name);
    if (checkFirstName.value === false) {
      return checkFirstName;
    }

    return {value: true, data: post, code: ''};
  }

  /**
   * Post edit check
   */
  public checkEditPost(post: any) {
    const checkTitle = this.checkTitle(post.title);
    if (checkTitle.value === false) {
      return checkTitle;
    }

    return {value: true, data: post, code: ''};
  }

}
