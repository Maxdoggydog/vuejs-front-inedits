export default class TinyMCEService {
  /**
   * Api key for tinymce 5
   */
  public api_key: string = process.env.VUE_APP_TINYMCE_API_KEY;

  /**
   * Whitelist for html tags
   */
  public whiteList: string = 'em | li | ol | p[class|style] | u | ul span[style] | strong | br | div | i | img[class|style|src|width|height]';

  /**
   * Config tinymce
   */
  public config: object = {
    selector: '.wysiwyg',
    menubar: false,
    statusbar: false,
    toolbar: 'undo redo | bold italic underline | numlist bullist | pagebreak',
    plugins: 'wordcount, lists, fullscreen, pagebreak, paste, pagebreak',
    pagebreak_separator: '<page pageset="old"></page>',
    extended_valid_elements: this.whiteList,
    language_url: 'https://inedits.fr/dist/langs/fr_FR.js',
    language: 'fr_FR',
    paste_as_text: true,
    height : 400,
  };
}
