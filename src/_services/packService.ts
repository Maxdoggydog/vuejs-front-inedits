import axios from 'axios';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

export default class PackService {

  public typePaper: any = {
    0 : 'Aucun',
    1 : '110 X 180',
    2 : '134 X 204',
    3 : '170 X 240',
    4 : '210 X 297',
    5 : '148 X 210',
    6 : 'Autre A5',
    7 : 'Autre A4',
    8 : 'Autre A3',
    9 : '85 X 55',
    10 : '120 X 180',
    11 : '210 X 148',
    12 : '115 X 200',
    13 : '200 X 200',
    14 : '210 X 270',
    15 : '150 X 220',
    16 : '150 x 190',
    17 : '190 x 150',
    18 : '210 x 420',
    20 : 'A4',
    21 : 'A4',
    22 : 'F6X9',
    23 : 'USLETTER',
    24 : 'F21X21',
    25 : '145X210',
    26 : '140X225',
  };

  public typeCouverture: any = {
    0 : 'Aucun',
    1 : 'Ivoire Texturé',
    2 : 'Satiné 250 grs',
    3 : 'Satiné 300 grs',
    4 : 'Mat 250 grs',
    5 : 'Mat 300 grs',
    6 : 'Synthetique 140 micron',
    7 : 'MagnoSatin 135grs',
    10 : 'MagnoSatin 135grs',
    11 : 'Invercote 240g MAT',
    12 : 'Invercote 240g SAT',
    22 : 'Satiné 200 grs',
  };

  public typeReliure: any = {
    0 : 'Aucun',
    1 : 'DCC',
    2 : 'Spirale',
    3 : 'Agrafé',
    4 : 'Chevalet',
    5 : 'DCC Couv rigide',
    6 : 'Carte',
    7 : 'Affiche / Flyers',
    8 : 'Carte de voeux',
    9 : 'Communique',
    10 : 'Bloc-note',
    11 : 'Trou De Classement',
    12 : 'Depliant',
    13 : 'DCC (tva:20%)',
    14 : 'Expédition SB Diffusion',
    15 : 'Flipbook',
    16 : 'Gratuit',
    20 : 'hardcover',
    21 : 'softcover',
    22 : 'hardcover - BLACK',
    23 : 'NUMERISATION',
    25 : 'Sous traitance',
  };

  public typeFormat: any = {
    0 : 'Aucun',
    1 : '110 X 180',
    2 : '134 X 204',
    3 : '170 X 240',
    4 : '210 X 297',
    5 : '148 X 210',
    6 : 'Autre A5',
    7 : 'Autre A4',
    8 : 'Autre A3',
    9 : '85 X 55',
    10 : '120 X 180',
    11 : '210 X 148',
    12 : '115 X 200',
    13 : '200 X 200',
    14 : '210 X 270',
    15 : '150 X 220',
    16 : '150 x 190',
    17 : '190 x 150',
    18 : '210 x 420',
    20 : 'A4',
    21 : 'A4',
    22 : 'F6X9',
    23 : 'USLETTER',
    24 : 'F21X21',
    25 : '145X210',
    26 : '140X225',
  };

  private options: object = {headers : _logInService.headers};

  /**
   * Get all Packs
   */
  public getAllPacks() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/packs/all', {});
  }

  /**
   * Auth for header
   */
  public authHeader() {
    // return authorization header with jwt token
    const user = JSON.parse(localStorage.getItem('user') || '{}');

    if (user && user.token) {
      return { Authorization: 'Bearer ' + user.token };
    } else {
      return {};
    }
  }

}
