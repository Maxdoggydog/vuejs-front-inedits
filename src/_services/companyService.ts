import axios from 'axios';
import Pack from '@/_classes/pack.class.ts';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

export default class CompanyService {

  private options: object = {headers : _logInService.headers};

  /**
   * Get all Company
   */
  public getAllCompanies() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/companies/all', {});
  }

  /**
   * Check value for Pack
   */
  public checkPack(selected_pack: Pack) {
    if (!selected_pack) {
      return {value: false, message: 'Pack is Null', code: 'pack_is_null'};
    } else if (selected_pack.slug === '') {
      return {value: false, message: 'Pack is empty', code: 'pack_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for Company name
   */
  public checkCompanyName(name: string) {
    if (!name) {
      return {value: false, message: 'Company name is Null', code: 'company_name_is_null'};
    } else if (name === '') {
      return {value: false, message: 'Company name is empty', code: 'company_name_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for Company adress
   */
  public checkInvoiceAdress(invoice_adress: string) {
    if (!invoice_adress) {
      return {value: false, message: 'Invoice adress is Null', code: 'invoice_adress_is_null'};
    } else if (invoice_adress === '') {
      return {value: false, message: 'Invoice adress is empty', code: 'company_adress_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for Company zip code
   */
  public checkInvoiceZipCode(invoice_zip_code: string) {
    if (!invoice_zip_code) {
      return {value: false, message: 'Invoice zip code is Null', code: 'invoice_zip_code_is_null'};
    } else if (invoice_zip_code === '') {
      return {value: false, message: 'Invoice zip code is empty', code: 'invoice_zip_code_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Check value for Company city
   */
  public checkInvoiceCity(invoice_city: string) {
    if (!invoice_city) {
      return {value: false, message: 'Invoice city is Null', code: 'invoice_city_is_null'};
    } else if (invoice_city === '') {
      return {value: false, message: 'Invoice city is empty', code: 'invoice_city_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }

  /**
   * Company check
   */
  public checkCompanyInfo(selected_pack: any, company: any) {
    const checkPack = this.checkPack(selected_pack);
    if (checkPack.value === false) {
      return checkPack;
    }
    const checkCompanyName = this.checkCompanyName(company.name);
    if (checkCompanyName.value === false) {
      return checkCompanyName;
    }
    const checkInvoiceAdress = this.checkInvoiceAdress(company.invoice_address);
    if (checkInvoiceAdress.value === false) {
      return checkInvoiceAdress;
    }
    const checkInvoiceZipCode = this.checkInvoiceZipCode(company.invoice_zip_code);
    if (checkInvoiceZipCode.value === false) {
      return checkInvoiceZipCode;
    }
    const checkInvoiceCity = this.checkInvoiceCity(company.invoice_city);
    if (checkInvoiceCity.value === false) {
      return checkInvoiceCity;
    }

    company.pack = selected_pack;
    return {value: true, data: company, code: ''};
  }

  /**
   * Create company
   */
  public createCompany(company: any) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/companies/create', company, this.options);
  }

  /**
   * Get user companies
   */
  public getUserCompanies(user_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/companies/' + user_id, this.options);
  }

  /**
   * Get user companies
   */
  public getCompany(slug: string) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/companies/' + slug, this.options);
  }

  /**
   * Get user companies with no posts
   */
  public getCompanieWithoutPost() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/companies/no/posts', this.options);
  }

}
