import User from '@/_classes/user.class.ts';

export default class InitMeta {
  // Use meta from path
  public initMeta(router: any) {
    router.beforeEach(
      (to: any, from: any, next: any) => {
        const nearestWithTitle = to.matched.slice().reverse().find(
          (r: any) => r.meta && r.meta.title,
        );

        const nearestWithMeta = to.matched.slice().reverse().find(
          (r: any) => r.meta && r.meta.metaTags,
        );
        const previousNearestWithMeta = from.matched.slice().reverse().find(
          (r: any) => r.meta && r.meta.metaTags,
        );

        if (nearestWithTitle) { document.title = nearestWithTitle.meta.title; }

        Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(
          (el: any) => el.parentNode.removeChild(el),
        );

        if (!nearestWithMeta) { return next(); }

        nearestWithMeta.meta.metaTags.map((tagDef: any) => {
          const tag = document.createElement('meta');

          Object.keys(tagDef).forEach( (key: any) => {
            tag.setAttribute(key, tagDef[key]);
          });

          tag.setAttribute('data-vue-router-controlled', '');

          return tag;
        })
        .forEach((tag: any) =>  document.head.appendChild(tag) );

        next();
      },
    );
  }

  // Check if user is logged
  public checkLoggedUser(router: any) {
    router.beforeEach((to: any, from: any, next: any) => {
      const publicPages = ['home', 'cgu', 'comment-ca-marche', 'prestations', 'reset-check-email', 'reset', 'login'];
      const authRequired = !publicPages.includes(to.name);
      const loggedIn = JSON.parse(localStorage.getItem('user') || '{}');

      if (authRequired && !loggedIn && !loggedIn.token) {
        return next('/login');
      }

      next();
    });
  }

  // Init metas for all pages
  public initRouterMeta(page: string, user?: any) {
    switch (page) {
      case 'cgu' :
        return this.cguMeta();
      case 'comment-ca-marche' :
        return this.commentCaMarcheMeta();
      case 'home' :
        return this.homeMeta();
      case 'login' :
        return this.loginMeta();
      case 'register' :
        return this.registerMeta();
      case 'reset' :
        return this.resetMeta();
      case 'reset-check-email' :
        return this.resetCheckEmailMeta();
      case 'register-check-email' :
        return this.registerCheckEmailMeta();
      case 'pack' :
        return this.packMeta();
      case 'profile' :
        return this.profileMeta(user);
      case 'profile' :
        return this.profileEditMeta();
      case 'company' :
        return this.companyMeta();
      case 'company-new' :
        return this.companyMeta();
      case 'company-show' :
        return this.companyMeta();
      case 'company-contributors' :
        return this.companyContributorsMeta();
      case 'trees' :
        return this.treesMeta();
      case 'tree-posts' :
        return this.treePostsMeta();
      case 'post-show' :
        return this.postShowMeta(user);
      case 'add' :
        return this.addMeta();
      case 'pdfs' :
        return this.pdfsMeta();
      case 'pdf-command' :
        return this.pdfCommandMeta();
      case 'selection' :
        return this.selectionMeta();
      case 'selection-show' :
        return this.selectionShowMeta();
      default:
        return this.nullMeta();
    }
  }

  // Init metas for cgu page
  public cguMeta() {
    return {
      title: 'Inédits - Conditions générales d\'utilisation',
      pageSentence: 'Conditions générales d\'utilisation',
      metaTags: [
        {
          name: 'description',
          content: 'Les conditions générales d\'Inedits',
        },
        {
          property: 'og:description',
          content: 'Les conditions générales d\'Inedits',
        },
      ],
    };
  }

  // Init metas for comment ça marche page
  public commentCaMarcheMeta() {
    return {
      title: 'Inédits - Comment ça marche',
      pageSentence: 'Comment ça marche',
      metaTags: [
        {
          name: 'description',
          content: 'Vous souhaitez participer à l\'écriture collaborative, voici une aide pour faire vos premiers pas sur le réseau social d’écriture.',
        },
        {
          property: 'og:description',
          content: 'Vous souhaitez participer à l\'écriture collaborative, voici une aide pour faire vos premiers pas sur le réseau social d’écriture.',
        },
      ],
    };
  }

  // Init metas for home page
  public homeMeta() {
    return {
      title: 'Inédits - Le premier site d’écriture collaborative',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Bienvenue sur notre site internet. Nous aimons découvrir de nouveaux talents littéraires, alors pourquoi pas vous ? Inscrivez-vous et inventez les histoires de demain.',
        },
        {
          property: 'og:description',
          content: 'Bienvenue sur notre site internet. Nous aimons découvrir de nouveaux talents littéraires, alors pourquoi pas vous ? Inscrivez-vous et inventez les histoires de demain.',
        },
      ],
    };
  }

  // Init metas for login page
  public loginMeta() {
    return {
      title: 'Inédits - Connectez vous à votre compte',
      pageSentence: 'Se connecter',
      metaTags: [
        {
          name: 'description',
          content: 'Accédez à votre compte sur notre site internet, le premier réseau social d\'écriture collaborative.',
        },
        {
          property: 'og:description',
          content: 'Accédez à votre compte sur notre site internet, le premier réseau social d\'écriture collaborative.',
        },
      ],
    };
  }

  // Init metas for register page
  public registerMeta() {
    return {
      title: 'Inédits - Créez votre compte',
      pageSentence: 'S\'inscrire',
      metaTags: [
        {
          name: 'description',
          content: 'Créez un compte sur le premier réseau social d\'écriture littéraire collaborative',
        },
        {
          property: 'og:description',
          content: 'Créez un compte sur le premier réseau social d\'écriture littéraire collaborative',
        },
      ],
    };
  }

  // Init metas for reset page
  public resetMeta() {
    return {
      title: 'Inédits - Réinitialiser votre mot de passe',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Réinitialiser votre mot de passe',
        },
        {
          property: 'og:description',
          content: 'Réinitialiser votre mot de passe',
        },
      ],
    };
  }

  // Init metas for reset page
  public resetCheckEmailMeta() {
    return {
      title: 'Inédits - Vérification email',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Vérification email',
        },
        {
          property: 'og:description',
          content: 'Vérification email',
        },
      ],
    };
  }

  // Init metas for reset page
  public registerCheckEmailMeta() {
    return {
      title: 'Inédits - Confirmation',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Confirmation email',
        },
        {
          property: 'og:description',
          content: 'Confirmation email',
        },
      ],
    };
  }

  // Init metas for prestation 'Pack' page
  public packMeta() {
    return {
      title: 'Inédits - Prestation',
      pageSentence: 'Les différentes offres',
      metaTags: [
        {
          name: 'description',
          content: 'Les différentes offres',
        },
        {
          property: 'og:description',
          content: 'Les différentes offres',
        },
      ],
    };
  }

  // Init metas for profile page
  public profileMeta(user: User) {
    const profile = user && user.user ? user : new User();
    return {
      title: 'Inédits - ' + profile.user.first_name + ' ' + profile.user.last_name,
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Découvrez les œuvres de ' + profile.user.first_name + ' ' + profile.user.last_name,
        },
        {
          property: 'og:description',
          content: 'Découvrez les œuvres de ' + profile.user.first_name + ' ' + profile.user.last_name,
        },
      ],
    };
  }

  // Init metas for edit profile page
  public profileEditMeta() {
    return {
      title: 'Inédits - Editer mon profil',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Editer profil',
        },
        {
          property: 'og:description',
          content: 'Editer profil',
        },
      ],
    };
  }

  // Init metas for company page
  public companyMeta() {
    return {
      title: 'Inédits - Mes groupes',
      pageSentence: 'Mes groupes',
      metaTags: [
        {
          name: 'description',
          content: 'Mes groupes auxquelles j\'ai accès',
        },
        {
          property: 'og:description',
          content: 'Mes groupes auxquelles j\'ai accès',
        },
      ],
    };
  }

  // Init metas for new company page
  public companyNewMeta() {
    return {
      title: 'Inédits - Mes groupes',
      pageSentence: 'Mes groupes',
      metaTags: [
        {
          name: 'description',
          content: 'Création d\'un nouveaux groupe',
        },
        {
          property: 'og:description',
          content: 'Création d\'un nouveaux groupe',
        },
      ],
    };
  }

  // Init metas to show company page
  public companyShowMeta() {
    return {
      title: 'Inédits - Mes groupes',
      pageSentence: 'Mes groupes',
      metaTags: [
        {
          name: 'description',
          content: 'Détail du groupe',
        },
        {
          property: 'og:description',
          content: 'Détail du groupe',
        },
      ],
    };
  }

  // Init metas to show company's contributors page
  public companyContributorsMeta() {
    return {
      title: 'Inédits - Mes groupes',
      pageSentence: 'Mes groupes: Contributeurs',
      metaTags: [
        {
          name: 'description',
          content: 'Tous les contributeurs',
        },
        {
          property: 'og:description',
          content: 'Tous les contributeurs',
        },
      ],
    };
  }

  // Init metas to show trees page
  public treesMeta() {
    return {
      title: 'Inédits - Les arbres en cours d’écriture',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Notre concept d\'écriture collaborative est original et novateur. Découvrez nos arbres et participez à l\'aventure !',
        },
        {
          property: 'og:description',
          content: 'Notre concept d\'écriture collaborative est original et novateur. Découvrez nos arbres et participez à l\'aventure !',
        },
      ],
    };
  }

  // Init metas to show trees page
  public treePostsMeta() {
    return {
      title: 'Inédits - Découvrez',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Découvrez sur le réseau social d\'écriture Inédits.',
        },
        {
          property: 'og:description',
          content: 'Découvrez sur le réseau social d\'écriture Inédits.',
        },
      ],
    };
  }

  // Init metas to show post page
  public postShowMeta(user: User) {
    const profile = user && user.user ? user : new User();
    return {
      title: 'Inédits - Contribution',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Découvrez la contribution par ' + profile.user.first_name + ' ' + profile.user.last_name,
        },
        {
          property: 'og:description',
          content: 'Découvrez la contribution par ' + profile.user.first_name + ' ' + profile.user.last_name,
        },
      ],
    };
  }

  // Init metas to show add post page
  public addMeta() {
    return {
      title: 'Inédits - Contribuer',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: 'Contribuer en ajoutant des texts ou images',
        },
        {
          property: 'og:description',
          content: 'Contribuer en ajoutant des texts ou images',
        },
      ],
    };
  }

  // Init metas to show pdf page
  public pdfsMeta() {
    return {
      title: 'Inédits - Mes livres',
      pageSentence: 'Mes livres',
      metaTags: [
        {
          name: 'description',
          content: 'Tous mes livres',
        },
        {
          property: 'og:description',
          content: 'Tous mes livres',
        },
      ],
    };
  }

  // Init metas to show pdf command page
  public pdfCommandMeta() {
    return {
      title: 'Inédits - Mes livres - commande',
      pageSentence: 'Mon livre commande',
      metaTags: [
        {
          name: 'description',
          content: 'Mon livre',
        },
        {
          property: 'og:description',
          content: 'Mon livre',
        },
      ],
    };
  }

  // Init metas to show selection
  public selectionMeta() {
    return {
      title: 'Inédits - Mes sélections',
      pageSentence: 'Mes sélections',
      metaTags: [
        {
          name: 'description',
          content: 'Mes sélections',
        },
        {
          property: 'og:description',
          content: 'Mes sélections',
        },
      ],
    };
  }

  // Init metas to show selection before pdf
  public selectionShowMeta() {
    return {
      title: 'Inédits - Mes sélections avant transformation',
      pageSentence: 'Mes sélections : Avant transformation',
      metaTags: [
        {
          name: 'description',
          content: 'Mes sélections',
        },
        {
          property: 'og:description',
          content: 'Mes sélections',
        },
      ],
    };
  }

  // Init metas for home page
  public nullMeta() {
    return {
      title: 'Inédits',
      pageSentence: '',
      metaTags: [
        {
          name: 'description',
          content: '',
        },
        {
          property: 'og:description',
          content: '',
        },
      ],
    };
  }
}
