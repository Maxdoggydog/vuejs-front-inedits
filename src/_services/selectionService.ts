import axios from 'axios';
import LogInService from '@/_services/logInService';

const _logInService = new LogInService();

export default class SelectionService {
  private options: object = {headers : _logInService.headers};

  /**
   * Auth for header
   */
  public authHeader() {
    // return authorization header with jwt token
    const user = JSON.parse(localStorage.getItem('user') || '{}');

    if (user && user.token) {
      return { Authorization: 'Bearer ' + user.token };
    } else {
      return {};
    }
  }

  /**
   * Get all Selection
   */
  public getAllSelections() {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/selections/all', this.options);
  }

  /**
   * Get all Selection by company
   */
  public getAllSelectionsByUserId(user_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/selections/user/' + user_id, this.options);
  }

  /**
   * Get all Posts by post_id
   */
  public getPdfById(post_id: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/selections/' + post_id, this.options);
  }

  /**
   * Get all Posts by slug
   */
  public getPdfBySlug(slug: any) {
    return axios.get(process.env.VUE_APP_API_DOMAINE + '/pro/selections/' + slug, this.options);
  }

  /**
   * Create pdf
   */
  public createPdf(pdf: object, company_id: any, parent_id?: any) {
    const api_url = process.env.VUE_APP_API_DOMAINE + '/pro/selections/company/' + company_id + '/' + parent_id;
    return axios.post(api_url, pdf, this.options);
  }

  /**
   * Sort selections
   */
  public updateSortSelections(post_id: any, selections_updated: any, start_pos: number, end_pos: any) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/selections/update/' + post_id + '/order', {selections_updated, start_pos, end_pos}, this.options);
  }

  /**
   * Check value for Post title
   */
  public checkTitle(title: string) {
    if (!title) {
      return {value: false, message: 'Title is null', code: 'title_is_null'};
    } else if (title === '') {
      return {value: false, message: 'Title is empty', code: 'title_is_empty'};
    } else {
      return {value: true, message: '', code: ''};
    }
  }



  /**
   * Post check
   */
  public checkPostInfo(pdf: any) {
    const checkTitle = this.checkTitle(pdf.title);
    if (checkTitle.value === false) {
      return checkTitle;
    }

    return {value: true, data: pdf, code: ''};
  }

  /**
   * Get selected post
   */
  public getSelectedPost(user_id: any, post_id: any, slug: string) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/selections/' + slug + '/selection', {user_id, post_id}, this.options);
  }

}
