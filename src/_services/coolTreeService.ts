import axios from 'axios';
import jquery from 'jquery';
import * as d3 from 'd3';
import d3Tip from 'd3-tip';
import { hierarchy, tree as d3Tree } from 'd3-hierarchy';
import { event as currentEvent } from 'd3';
import LogInService from '@/_services/logInService';
import FrontEndService from '@/_services/frontEndService';

const _logInService = new LogInService();
const _d3Tip: any = d3Tip;
const d3tip: any = new _d3Tip();
const _d3Tree: any = d3Tree;
const d3tree: any = new _d3Tree();
const frontEndService = new FrontEndService();

export default class CoolTreeService {

  public init(obj: any) {
    const options: object = {headers : _logInService.headers};
    const rootUrl: string = process.env.VUE_APP_API_DOMAINE;
    const duration = 750;
    let root: any = {};
    let isAdmin: boolean = false;
    let i: number = 0;
    const margin = {top: 0, right: 0, bottom: 0, left: 10};
    const width = window.innerWidth - margin.right - margin.left;
    const height = window.innerHeight - margin.top - margin.bottom;
    const currentUserId = obj.currentUserId;

    const tip = d3tip.attr('class', 'd3-tip').offset([-10, 0]).html((d: any) => {
      const index = d.id;
      const parentElement = $('#post_' + index).parent().get(0);
      return d3.select(parentElement).attr('title');
    });

    const drag = d3.drag().subject((d: any) =>  d ).on('start', dragstarted).on('drag', dragged).on('end', dragended);

    const zoom = d3.zoom().scaleExtent([1, 10]).on('zoom', zoomed);

    const tree: any = d3.tree().size([height / 1.2, width]);

    const diagonal = function link(d: any) {
      return 'M' + d.source.y + ',' + d.source.x
        + 'C' + (d.source.y + d.target.y) / 2 + ',' + d.source.x
        + ' ' + (d.source.y + d.target.y) / 2 + ',' + d.target.x
        + ' ' + d.target.y + ',' + d.target.x;
    };

    const svg = d3.select('.tree-container').append('svg')
      .attr('width', width + margin.right + margin.left)
      .attr('height', height + margin.top + margin.bottom)
      .call(zoom)
      .append('g')
      .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
      .attr('id', 'svg');

    svg.call(tip);

    d3.json(obj.labelJSON, options)
      .then((data: any) => {
        root = data.theTree[0];
        root.x0 = height / 2;
        root.y0 = 0;
        isAdmin = data.isAdmin;

        function collapse(d: any) {
          if (d.children) {
            d._children = d._children;
            d._children.forEach(collapse);
            d._children = null;
          }
        }

        update(root, root.x0);
      })
      .catch((error: any) => {
        throw error;
      });

    d3.select(self.frameElement).style('height', '800px');

    function update(source: any, root_x0: number) {
      // Compute the new tree layout.
      const treeRoot = hierarchy(root);
      d3tree(treeRoot);
      const nodes = treeRoot.descendants();
      const links = treeRoot.links();

      // Normalize for fixed-depth.
      nodes.forEach((d: any) => {
        d.x = root_x0;
        d.y = d.depth * 180;
      });

      // Update the nodes…
      const node = svg.selectAll('g.node').data(nodes, (d: any) => d.id || (d.id = ++i) );

      // Enter any new nodes at the parent's previous position.
      const nodeEnter = node.enter().append('g')
        .attr('class', (d: any) => {
          return 'node';
        })
        .attr('transform', (d: any) =>  'translate(' + source.y0 + ',' + source.x0 + ')' )
        .on('mouseover', hover)
        .on('mouseout', hoverOut);

      const node2 = svg.selectAll('g.node').data(nodes, (d: any) => d.id || (d.id = ++i) ); // TODO:to fix || I don't why but do the trick

      // Node of the post
      nodeEnter.append('circle')
        // .attr('r', (d: any) => { if (d.parent === null) { return 40; } else { return 10; } })
        .attr('r', (d: any) => 10 )
        .attr('slug', (d: any) => d.data.data.slug )
        .attr('id', (d: any) =>  'post_' + d.id )
        .attr('class', (d: any) => { if (d.data.data.selection_id) { return 'child-selected'; } else { return d.data.data.file ? 'child-img' : 'child'; } } )
        .on('click', clickShow);

      nodeEnter
        .filter((d: any) => d.data.data.user_profile === currentUserId )
        .append('path')
        .attr('slug', (d: any) =>  d.data.data.slug )
        .attr('d', 'M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z')
        .attr('transform', 'scale(0.02, 0.02) translate(-220, -240)')
        .attr('style', 'fill:black')
        .on('click', clickShow);

      // Add post to the list of selection
      // show the btn only to the admin
      if (isAdmin) {
        nodeEnter.append('image')
          .attr('xlink:href', (d: any) => { if (d.data.data.selection_id) { return require('./../assets/images/trees/substraction.svg'); } else { return require('./../assets/images/trees/addition.svg'); } } )
          .attr('x', -22)
          .attr('y', -70)
          .attr('width', 40)
          .attr('height', 40)
          .attr('slug', (d: any) => d.data.data.slug )
          .attr('id', (d: any) => 'action_' + d.id )
          .attr('data-toggle', (d: any) => 'tooltip' )
          .attr('data-placement', (d: any) => 'top' )
          .attr('alt', (d: any) => { if (d.data.data.selection_id) { return 'Retirer de ma sélection'; } else { return 'Ajouter à ma sélection'; } } )
          .attr('title', (d: any) => { if (d.data.data.selection_id) { return 'Retirer de ma sélection'; } else { return 'Ajouter à ma sélection'; } } )
          .attr('class', (d: any) => 'child-links child-links-' + d.id )
          .on('click', clickAddToMySelection)
          .on('mouseover', tip.show)
          .on('mouseout', tip.hide);
      }

      // Transition nodes to their new position.
      const nodeUpdate = node2.transition()
        .duration(duration)
        .attr('transform', (d: any) => 'translate(' + d.y + ',' + d.x + ')' );

      nodeUpdate.select('circle').attr('r', 10);

      nodeUpdate.select('text').style('fill-opacity', 1);

      // Transition exiting nodes to the parent's new position.
      const nodeExit = node2.exit().transition()
        .duration(duration)
        .attr('transform', (d: any) => 'translate(' + source.y + ',' + source.x + ')' )
        .remove();

      nodeExit.select('circle').attr('r', (d: any) => { if (d.parent === null) { return 20; } else { return 10; } });

      nodeExit.select('text').style('fill', (d: any) => 'url(#' + d.data.data.user.slug + ')' );

      // Update the links…
      const link = svg.selectAll('path.link').data(links, (d: any) =>  d.target.id );

      // Enter any new links at the parent's previous position.
      link.enter().insert('path', 'g')
        .attr('class', 'link')
        .attr('d', (d: any) => {
          const o = {x: source.x0, y: source.y0};
          return diagonal({source: o, target: o});
        });

      const link2 = svg.selectAll('path.link').data(links, (d: any) =>  d.target.id ); // TODO:to fix || I don't why but do the trick

      // Transition links to their new position.
      link2.transition().duration(duration).attr('d', diagonal);

      // Transition exiting nodes to the parent's new position.
      link2.exit().transition()
        .duration(duration)
        .attr('d', (d: any) => {
          const o = {x: source.x, y: source.y};
          return diagonal({source: o, target: o});
        })
        .remove();

      // Stash the old positions for transition.
      nodes.forEach((d: any) => {
        d.x0 = d.x;
        d.y0 = d.y;
      });
    }

    // Toggle children on click.
    function updateClick(d: any) {
      if (d.children) {
        d._children = d.children;
        d.children = null;
      } else {
        d.children = d._children;
        d._children = null;
      }
      update(d, d.x0);
    }

    function clickShow(d: any) {
      window.location.href = document.location.origin + '/tree/show/' + d.id;
    }

    /**
     * Add the post to the selection
     */
    function clickAddToMySelection(d: any) {
      axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/posts/tree/' + d.id + '/selection', {post_id: d.id}, options)
        .then((response: any) => {
          console.log('response', response);
          const circlePost: any = document.getElementById('post_' + d.id);
          const circleAction: any = document.getElementById('action_' + d.id);
          if (response.data.action === 'add') {
            $(circlePost).addClass('child-selected');
            const classe = d.file ? 'child-img' : 'child';
            $(circlePost).removeClass(classe);
            $(circleAction).attr('title', 'Retirer de ma sélection');
            $(circleAction).attr('alt', 'Retirer de ma sélection');
            $(circleAction).attr('href', require('./../assets/images/trees/substraction.svg'));
          } else {
            const classe = d.file ? 'child-img' : 'child';
            $(circlePost).addClass(classe);
            $(circlePost).removeClass('child-selected');
            $(circleAction).attr('title', 'Ajouter à ma sélection');
            $(circleAction).attr('alt', 'Ajouter à ma sélection');
            $(circleAction).attr('href', require('./../assets/images/trees/addition.svg'));
          }
        })
        .catch((error: any) => {
          console.log('error', error);
        });
    }

    // Hover Bubble
    function hover(d: any) {
      const index       = d.id;
      const classe      = '.child-links-' + index;
      const parentElement = $('#post_' + index).parent().get(0);
      const childBuble: any  = document.getElementById(index);
      const childLink   = d3.selectAll(classe);
      const activeChildBuble = $('.child-bubble.onScene');

      d3.selectAll('.hover').classed('hover', false);
      d3.select(parentElement).classed('hover', true);
      activeChildBuble.removeClass('onScene');

      $(childBuble).addClass('onScene');
      childLink.classed('onScene', true);
      frontEndService.initTooltip();
    }

    function hoverOut(d: any) {
      const index   = d.id;
      const classe      = '.child-links-' + d.id;
      const childBuble  = document.getElementById(index);
      const childLink   = d3.selectAll(classe);

      childLink.classed('onScene', false);
    }

    function zoomed() {
      svg.attr('transform', currentEvent.transform);
    }

    function dragstarted(d: any) {
      console.log('dragstarted', d);
      const index = d.id;
      const parentElement = $('#post_' + index).parent().get(0);
      currentEvent.sourceEvent.stopPropagation();
      d3.select(parentElement).classed('dragging', true);
    }

    function dragged(d: any) {
      console.log('dragged', d);
      const index = d.id;
      const parentElement = $('#post_' + index).parent().get(0);
      d3.select(parentElement).attr('cx', d.x = currentEvent.x).attr('cy', d.y = currentEvent.y);
    }

    function dragended(d: any) {
      console.log('dragended', d);
      const index = d.id;
      const parentElement = $('#post_' + index).parent().get(0);
      d3.select(parentElement).classed('dragging', false);
    }

    // Init help icon
    d3
      .select('#tree-help-icon')
      .on('mouseover', () => {
        d3.select('#tree-help-bubble').classed('onScene', true);
      })
      .on('mouseout', () => {
        d3.select('#tree-help-bubble').classed('onScene', false);
      })
    ;
  }

}

