import axios from 'axios';

export default class LogInService {
  public headers: object = {
    'Access-Control-Allow-Origin': process.env.VUE_APP_API_DOMAINE,
    'Access-Control-Allow-Methods': 'POST, GET',
    'Accept': 'application/json;',
    'withCredentials': true,
    'Authorization': this.authHeader().Authorization,
  };
  private options: object = {headers : this.headers};

  /**
   * Get logged
   */
  public getLogIn(log_params: object) {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/login/api-token-auth-inedits/', log_params, {headers : this.headers});
  }

  /**
   * Auth for header
   */
  public authHeader() {
    // return authorization header with jwt token
    const user = JSON.parse(localStorage.getItem('user') || '{}');

    if (user && user.token) {
      return { Authorization: 'Bearer ' + user.token };
    } else {
      return {};
    }
  }

  /**
   * Loggout
   */
  public getLogOut() {
    return axios.post(process.env.VUE_APP_API_DOMAINE + '/pro/login/logout/', {}, {headers: this.headers});
  }

  /**
   *
   */
  public getAll() {
    const requestOptions: any = {
      method: 'GET',
      headers: this.authHeader(),
    };

    return axios.get(process.env.VUE_APP_API_DOMAINE + '/users', requestOptions);
  }

}
